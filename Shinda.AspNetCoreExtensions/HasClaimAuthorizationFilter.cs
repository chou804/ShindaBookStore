﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Shinda.AspNetCoreExtionsions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using System.Collections.Concurrent;
using System.Security.Claims;

namespace Shinda.AspNetCoreExtensions
{



    /// <summary>
    /// MVC HasCliam filter
    /// </summary>
    public class HasClaimAuthorizationFilter : IAsyncAuthorizationFilter
    {
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var attributes = context.ActionDescriptor.EndpointMetadata
                .Where(t => t.GetType() == typeof(HasClaimAttribute)).ToList();

            if (attributes?.Count() > 0)
            {
                if (!context.HttpContext.User.Identity.IsAuthenticated)
                {
                    await context.HttpContext.ChallengeAsync();
                    return;
                }

                var methodInfo = context.ActionDescriptor.GetType().GetProperty("MethodInfo")?.GetValue(context.ActionDescriptor, null) as System.Reflection.MethodInfo;
                if (methodInfo != null)
                {
                    //先檢查 action 的 HasClaim
                    var actionAttributes = methodInfo.GetCustomAttributes(typeof(HasClaimAttribute), false) as IEnumerable<HasClaimAttribute>;
                    if (!IsAuthorized(context.HttpContext, actionAttributes))
                    {
                        //如果 aciton 的未通過，則檢查 controller
                        var controllerAttributes = methodInfo.DeclaringType.GetCustomAttributes(typeof(HasClaimAttribute), false) as IEnumerable<HasClaimAttribute>;
                        if (!IsAuthorized(context.HttpContext, controllerAttributes))
                        {
                            await context.HttpContext.ForbidAsync();
                            return;
                        }
                    }
                }
                else
                {
                    // methodInfo is null => RazorPage's PageModel

                    var hasClaimAttributes = attributes.Cast<HasClaimAttribute>();
                    if (!IsAuthorized(context.HttpContext, hasClaimAttributes))
                    {
                        await context.HttpContext.ForbidAsync();
                        return;
                    }
                }
            }
        }

        private bool IsAuthorized(HttpContext httpContext, IEnumerable<HasClaimAttribute> hasClaimAttributes)
        {
            if (hasClaimAttributes == null)
                return false;

            foreach (var attr in hasClaimAttributes)
            {
                // [HasClaim("type")
                if (attr.ClaimValues == null)
                {
                    if (httpContext.User.HasClaim(t => t.Type == attr.ClaimType))
                        return true;
                }

                // [HasClaim("type", "value1", "value2"...]
                foreach (var claim in httpContext.User.Claims.Where(t => t.Type == attr.ClaimType))
                {
                    if (attr.ClaimValues.Contains(claim.Value))
                        return true;
                }
            }

            return false;
        }
    }
}
