﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shinda.AspNetCoreExtionsions
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true)]
    public class HasClaimAttribute : Attribute
    {
        public readonly string ClaimType;
        public readonly string[] ClaimValues;

        public HasClaimAttribute(string claimType) => ClaimType = claimType;
        public HasClaimAttribute(string claimType, params string[] claimValues) : this(claimType) => ClaimValues = claimValues;
    }

}
