﻿# Startup.cs
In ConfigureServices(IServiceCollection services) add

```csharp
services.AddClaimBaseAuthorization();
```

In Configure() after app.UseAuthorization() add

```csharp
app.UseClaimBaseAuthorization();
```

# usage


