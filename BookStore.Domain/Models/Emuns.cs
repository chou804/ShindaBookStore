﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Domain.Models
{
    public enum SaleState
    {
        /// <summary>
        /// 不可銷售
        /// </summary>
        NotForSale = 0,
        /// <summary>
        /// 可銷售
        /// </summary>
        ForSale = 1,
    }
}
