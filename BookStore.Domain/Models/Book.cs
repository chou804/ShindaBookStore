﻿using BookStore.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Domain.Models
{
    /// <summary>
    /// 書籍
    /// </summary>
    public class Book
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 書名
        /// </summary>
        public string BookName { get; set; }
        /// <summary>
        /// ISBN
        /// </summary>
        public string ISBN { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// 出版社
        /// </summary>
        public string Publisher { get; set; }
        /// <summary>
        /// 訂價
        /// </summary>
        public decimal MarketPrice { get; set; }
        /// <summary>
        /// 分類
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// 簡介
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 圖片
        /// </summary>
        public virtual List<BookImage> BookImages { get; set; } = new List<BookImage>();
        private int _quantity;
        /// <summary>
        /// 數量, 不得小於0
        /// </summary>
        public int Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (value < 0)
                    throw new BookStoreDomainException("商品數量不得小於0");

                _quantity = value;
            }
        }
        /// <summary>
        /// 銷售狀態
        /// </summary>
        public SaleState SaleStatus { get; set; }
        /// <summary>
        /// 建立者, 預設 system
        /// </summary>
        public string CreateBy { get; set; } = "system";
        /// <summary>
        /// 建立時間,預設UtcNow
        /// </summary>
        public DateTime CreateOn { get; set; } = DateTime.UtcNow;
        /// <summary>
        /// 修改者
        /// </summary>
        public string UpdateBy { get; set; }
        /// <summary>
        /// 修改時間
        /// </summary>
        public DateTime? UpdateOn { get; set; }
    }

    /// <summary>
    /// 書籍圖片
    /// </summary>
    public class BookImage
    {
        public string Id { get; set; }
        /// <summary>
        /// 圖片屬於哪一個Book
        /// </summary>
        public string BookId { get; set; }
        /// <summary>
        /// 圖片檔案名稱
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 圖片的public url
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 啟用
        /// </summary>
        public bool Enable { get; set; } = true;
    }
}
