﻿using BookStore.Domain.Models;
using BookStore.Domain.Repositories;
using BookStore.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;

namespace BookStore.Domain
{
    public class BookManager
    {
        readonly IBookRepository _bookRepository;
        readonly IImageStorage _imageStorage;
        readonly IImageProcessor _imageProcessor;

        /*透過 constructor injection 將相關依賴項目*/
        public BookManager(
            IBookRepository bookRepository,
            IImageStorage imageStorage,
            IImageProcessor imageProcessor)
        {
            Ensure.ArgumentNotEmpty(bookRepository, nameof(bookRepository));
            Ensure.ArgumentNotEmpty(imageStorage, nameof(imageStorage));
            Ensure.ArgumentNotEmpty(imageProcessor, nameof(imageProcessor));

            _bookRepository = bookRepository;
            _imageStorage = imageStorage;
            _imageProcessor = imageProcessor;
        }

        public void CreateBook(Book book)
        {
            Ensure.ArgumentNotEmpty(book, nameof(book));
            //TODO:做一些欄位的檢查

            _bookRepository.Insert(book);
            _bookRepository.SaveChanges();
        }

        /// <summary>
        /// 建立 BookImage 並將來源圖片存到圖片儲存庫
        /// </summary>
        /// <param name="sourceImageFilePath"></param>
        /// <returns></returns>
        public BookImage CreateBookImage(string sourceImageFilePath)
        {
            _imageProcessor.Compress(sourceImageFilePath);

            var img = new BookImage
            {
                Id = SequentialGuid.New(),
                Enable = true,
            };
            _imageStorage.SaveBookImage(sourceImageFilePath, img);

            return img;
        }

        public IQueryable<Book> GetBooks(Expression<Func<Book, bool>> filter = null)
        {
            return _bookRepository.Get(filter);
        }

        public Book GetBookById(string id)
        {
            return _bookRepository.GetByID(id);
        }

        public void UpdateBook(Book book)
        {
            _bookRepository.Update(book);
            _bookRepository.SaveChanges();
        }
    }
}
