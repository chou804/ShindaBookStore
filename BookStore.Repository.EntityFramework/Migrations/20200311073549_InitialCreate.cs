﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStore.Repository.EntityFramework.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Book",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 40, nullable: false),
                    BookName = table.Column<string>(maxLength: 500, nullable: false),
                    ISBN = table.Column<string>(maxLength: 20, nullable: false),
                    Author = table.Column<string>(maxLength: 300, nullable: false),
                    Publisher = table.Column<string>(maxLength: 300, nullable: true),
                    MarketPrice = table.Column<decimal>(type: "decimal(9,0)", nullable: false),
                    Category = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    SaleStatus = table.Column<int>(nullable: false),
                    CreateBy = table.Column<string>(maxLength: 200, nullable: true),
                    CreateOn = table.Column<DateTime>(type: "datetime2(3)", nullable: false),
                    UpdateBy = table.Column<string>(maxLength: 200, nullable: true),
                    UpdateOn = table.Column<DateTime>(type: "datetime2(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Book", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BookImage",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 40, nullable: false),
                    BookId = table.Column<string>(maxLength: 40, nullable: false),
                    FileName = table.Column<string>(maxLength: 200, nullable: false),
                    ImageUrl = table.Column<string>(maxLength: 500, nullable: false),
                    Enable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookImage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BookImage_Book_BookId",
                        column: x => x.BookId,
                        principalTable: "Book",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Book_ISBN",
                table: "Book",
                column: "ISBN",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BookImage_BookId",
                table: "BookImage",
                column: "BookId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BookImage");

            migrationBuilder.DropTable(
                name: "Book");
        }
    }
}
