﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Repository.EntityFramework
{
    public class BookStoreEFRepositoryExcpetion: Exception
    {
        public BookStoreEFRepositoryExcpetion() { }
        public BookStoreEFRepositoryExcpetion(string message) : base(message) { }
        public BookStoreEFRepositoryExcpetion(string message, Exception inner) : base(message, inner) { }
    }
}
