﻿using BookStore.Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Repository.EntityFramework
{
    public class BookStoreDbContext : IdentityDbContext<IdentityUser, IdentityRole, string>
    {
        public BookStoreDbContext(DbContextOptions<BookStoreDbContext> options) : base(options) { }

        public DbSet<Book> BooksSet { get; set; }
        public DbSet<BookImage> BookImagesSet { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*參考 
             * https://docs.microsoft.com/en-us/ef/core/modeling/entity-properties?tabs=data-annotations%2Cwithout-nrt
             */

            BookBuilder(modelBuilder);
            BookImageBuilder(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private void BookBuilder(ModelBuilder builder)
        {
            builder.Entity<Book>(eb =>
            {
                eb.ToTable("Book");
                eb.HasKey(c => c.Id);
                eb.Property(c => c.Id).HasMaxLength(40);

                eb.Property(c => c.BookName).HasMaxLength(500).IsRequired();
                eb.Property(c => c.ISBN).HasMaxLength(20).IsRequired();
                eb.Property(c => c.Author).HasMaxLength(300).IsRequired();
                eb.Property(c => c.MarketPrice).HasColumnType("decimal(9,0)");
                eb.Property(c => c.Publisher).HasMaxLength(300);
                eb.Property(c => c.Category).HasMaxLength(50);
                eb.Property(c => c.CreateBy).HasMaxLength(200);
                eb.Property(c => c.CreateOn).HasColumnType("datetime2(3)").IsRequired();
                eb.Property(c => c.UpdateBy).HasMaxLength(200);
                eb.Property(c => c.UpdateOn).HasColumnType("datetime2(3)");
            });

            builder.Entity<Book>()
                .HasIndex(c => c.ISBN)
                .IsUnique();


            // Single navigation property
            //builder.Entity<Book>().HasMany(c => c.BookImages);
            // Owned Entity Types
            //builder.Entity<Book>().OwnsMany(c => c.BookImages);
        }

        private void BookImageBuilder(ModelBuilder builder)
        {
            builder.Entity<BookImage>(eb =>
            {
                eb.ToTable("BookImage");
                eb.HasKey(c => c.Id);
                eb.Property(c => c.Id).HasMaxLength(40);

                eb.Property(c => c.BookId).HasMaxLength(40).IsRequired();
                eb.Property(c => c.FileName).HasMaxLength(200).IsRequired();
                eb.Property(c => c.ImageUrl).HasMaxLength(500).IsRequired();
            });

            //builder.Entity<BookImage>()
            //    .HasOne(c => c.Book)
            //    .WithMany(c => c.BookImages);
        }
    }
}
