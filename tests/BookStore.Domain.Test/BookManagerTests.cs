﻿using BookStore.Domain;
using BookStore.Domain.Models;
using BookStore.Domain.Repositories;
using BookStore.Repository.EntityFramework;
using LightInject;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using BookStore.Domain.Services;

namespace BookStore.Domain.Test
{
    public class BookManagerTests
    {
        ServiceProvider _serviceProvider;

        const string FAKE_BOOK_ID = "aaaaaaaa-0000-0000-0000-aaaaaaaaaaaa";

        readonly IBookRepository _bookRepository;
        readonly IImageStorage _imageStorage;
        readonly IImageProcessor _imageProcessor;

        BookStoreDbContext dbContext;
        ServiceContainer container;

        public BookManagerTests()
        {
            var services = new ServiceCollection();
            services.AddDbContext<BookStoreDbContext>(options =>
            {
                options.UseInMemoryDatabase(databaseName: "testdb");
            }, ServiceLifetime.Singleton);

            services.AddSingleton<IBookRepository, EFBookRepository>();
            services.AddSingleton<IImageProcessor, FakeImageProcessor>();
            services.AddSingleton<IImageStorage, FakeImageStorage>();
            services.AddSingleton<BookManager>();

            _serviceProvider = services.BuildServiceProvider();

            var db = _serviceProvider.GetService<BookStoreDbContext>();
            db.Database.EnsureCreated();
        }

        [Fact]
        public void Create_Book_Test()
        {
            var abook = new Book
            {
                Id = FAKE_BOOK_ID,
                BookName = "Troy1",
                ISBN = RandomISBN(),
                Author = "Troy",
                Publisher = "TroyLib",
                MarketPrice = 100,
                Category = "Programming",
                Description = "The best book on the earth",
                Quantity = 10,
                SaleStatus = SaleState.ForSale,
                BookImages = new List<BookImage>()
            };
            abook.BookImages.Add(new BookImage
            {
                Id = SequentialGuid.New(),
                BookId = abook.Id,
                FileName = "pic1.png",
                ImageUrl = "https://book.com/pic1.png"
            });
            abook.BookImages.Add(new BookImage
            {
                Id = SequentialGuid.New(),
                BookId = abook.Id,
                FileName = "pic2.png",
                ImageUrl = "https://book.com/pic2.png"
            });

            var bookManager = _serviceProvider.GetService<BookManager>();
            bookManager.CreateBook(abook);

            var bookInDb = bookManager.GetBookById(abook.Id);
            Assert.Equal("Troy1", bookInDb.BookName);
        }

        private string RandomISBN()
        {
            var rnd = new Random();
            return string.Format("{0}-{1}-{2}-{3}-{4}",
                rnd.Next(0, 999).ToString("000"),
                rnd.Next(0, 9),
                rnd.Next(0, 99).ToString("00"),
                rnd.Next(0, 999999).ToString("000000"),
                rnd.Next(0, 9)
                );
        }
    }
}
