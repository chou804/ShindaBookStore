using BookStore.Domain;
using BookStore.Domain.Models;
using BookStore.Domain.Repositories;
using BookStore.Repository.EntityFramework;
using LightInject;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Xunit;
using BookStore.Test.TestHelpers;
using System.Linq;

namespace BookStore.Test
{
    [TestCaseOrderer("BookStore.Test.TestHelpers.PriorityOrderer", "BookStore.Test")]
    public class EFBookRepositoryTests
    {
        const string FAKE_BOOK_ID = "aaaaaaaa-0000-0000-0000-aaaaaaaaaaaa";
        BookStoreDbContext dbContext;
        ServiceContainer container;

        public EFBookRepositoryTests()
        {
            var options = new DbContextOptionsBuilder<BookStoreDbContext>()
                            .UseInMemoryDatabase(databaseName: "testdb")
                            //.UseSqlServer("Server=.;Database=DemoTest1;User Id=sa;Password=sasa;MultipleActiveResultSets=true")
                            .UseLoggerFactory(new LoggerFactory(new[] { new Microsoft.Extensions.Logging.Debug.DebugLoggerProvider() }))
                            .UseLazyLoadingProxies() // refs. https://docs.microsoft.com/en-us/ef/core/querying/related-data
                            .Options;

            dbContext = new BookStoreDbContext(options);
            dbContext.Database.EnsureCreated();

            container = new ServiceContainer();
            container.RegisterInstance(dbContext);

            container.Register<IBookRepository, EFBookRepository>();
        }

        [Fact, TestPriority(1)]
        public void Insert_Then_GetById()
        {
            var abook = new Book
            {
                Id = FAKE_BOOK_ID,
                BookName = "Troy1",
                ISBN = RandomISBN(),
                Author = "Troy",
                Publisher = "TroyLib",
                MarketPrice = 100,
                Category = "Programming",
                Description = "The best book on the earth",
                Quantity = 10,
                SaleStatus = SaleState.ForSale,
                BookImages = new List<BookImage>()
            };
            abook.BookImages.Add(new BookImage
            {
                Id = SequentialGuid.New(),
                BookId = abook.Id,
                FileName = "pic1.png",
                ImageUrl = "https://book.com/pic1.png"
            });
            abook.BookImages.Add(new BookImage
            {
                Id = SequentialGuid.New(),
                BookId = abook.Id,
                FileName = "pic2.png",
                ImageUrl = "https://book.com/pic2.png"
            });

            var bookRepository = container.GetInstance<IBookRepository>();
            bookRepository.Insert(abook);
            bookRepository.SaveChanges();

            var bookInDb = bookRepository.GetByID(abook.Id);
            Assert.Equal("Troy1", bookInDb.BookName);
        }

        [Fact, TestPriority(2)]
        public void GetByID_With_TwoBookImages()
        {
            var bookRepository = container.GetInstance<IBookRepository>();
            var abook = bookRepository.GetByID(FAKE_BOOK_ID);
            Assert.Equal("Troy1", abook.BookName);
            Assert.Equal(2, abook.BookImages.Count);
        }

        [Fact, TestPriority(3)]
        public void IQueryable_WhereIf()
        {
            var bookRepository = container.GetInstance<IBookRepository>();
            var query = bookRepository.Get()
                        .WhereIf(true, t => t.BookName.Contains("T"))
                        .WhereIf(true, t => t.Quantity > 1);
            var books = query.ToList();

            Assert.True(books.Count > 0);
        }

        [Fact, TestPriority(4)]
        public void Update_BookName()
        {
            var bookRepository = container.GetInstance<IBookRepository>();
            var book = bookRepository.GetByID(FAKE_BOOK_ID);
            book.BookName = "Troy0";
            bookRepository.Update(book);
            bookRepository.SaveChanges();

            var book2 = bookRepository.GetByID(FAKE_BOOK_ID);
            Assert.Equal("Troy0", book2.BookName);

            book2.BookName = "Troy1";
            bookRepository.Update(book2);
            bookRepository.SaveChanges();
        }

        [Fact, TestPriority(5)]
        public void Update_BookImages()
        {
            var bookRepository = container.GetInstance<IBookRepository>();
            var book = bookRepository.GetByID(FAKE_BOOK_ID);
            book.BookImages.RemoveAll(t => t.FileName == "pic2.png");

            bookRepository.Update(book);
            bookRepository.SaveChanges();

            var book2 = bookRepository.GetByID(FAKE_BOOK_ID);
            Assert.Single(book2.BookImages);

            book2.BookImages.Add(new BookImage
            {
                Id = SequentialGuid.New(),
                BookId = book.Id,
                FileName = "pic2.png",
                ImageUrl = "https://book.com/pic2.png"
            });
            bookRepository.Update(book2);
            bookRepository.SaveChanges();
        }

        private string RandomISBN()
        {
            var rnd = new Random();
            return string.Format("{0}-{1}-{2}-{3}-{4}",
                rnd.Next(0, 999).ToString("000"),
                rnd.Next(0, 9),
                rnd.Next(0, 99).ToString("00"),
                rnd.Next(0, 999999).ToString("000000"),
                rnd.Next(0, 9)
                );
        }
    }
}
