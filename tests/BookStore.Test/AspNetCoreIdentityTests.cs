﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BookStore.Domain.Repositories;
using BookStore.Repository.EntityFramework;
using BookStore.Test.TestHelpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Xunit;

namespace BookStore.Test
{
    [TestCaseOrderer("BookStore.Test.TestHelpers.PriorityOrderer", "BookStore.Test")]
    public class AspNetCoreIdentityTests
    {
        private string UID { get { return Guid.NewGuid().ToString("N"); } }

        private string FAKE_USER_EMAIL { get { return $"user_{UID}@test.com"; } }
        private string FAKE_ROLENAME_ADMIN { get { return $"adminRole_{UID}"; } }

        private readonly IServiceProvider _serviceProvider;

        public AspNetCoreIdentityTests()
        {
            var options = new DbContextOptionsBuilder<BookStoreDbContext>()
                            .UseInMemoryDatabase(databaseName: "testdb")
                            //.UseSqlServer("Server=.;Database=DemoTest1;User Id=sa;Password=sasa;MultipleActiveResultSets=true")
                            .UseLoggerFactory(new LoggerFactory(new[] { new Microsoft.Extensions.Logging.Debug.DebugLoggerProvider() }))
                            .UseLazyLoadingProxies() // refs. https://docs.microsoft.com/en-us/ef/core/querying/related-data
                            .Options;

            var services = new ServiceCollection();

            services.AddDbContext<BookStoreDbContext>(options =>
            {
                options.UseInMemoryDatabase(databaseName: "testdb");
                //options.UseSqlServer("Server=.;Database=DemoTest1;User Id=sa;Password=sasa;MultipleActiveResultSets=true");
                options.UseLoggerFactory(new LoggerFactory(new[] { new Microsoft.Extensions.Logging.Debug.DebugLoggerProvider() }));
                options.UseLazyLoadingProxies();
            });

            services.AddIdentity<IdentityUser, IdentityRole>(options =>
            {
                // 設定User密碼複製度
                options.Password.RequireNonAlphanumeric = false;
            })
            .AddEntityFrameworkStores<BookStoreDbContext>()
            .AddDefaultTokenProviders();

            services.AddLogging();

            _serviceProvider = services.BuildServiceProvider();

            var db = _serviceProvider.GetService<BookStoreDbContext>();
            db.Database.EnsureCreated();
        }

        [Fact, TestPriority(1)]
        public async Task CreateUser_CheckNormalizedUserName()
        {
            var userManager = _serviceProvider.GetService<UserManager<IdentityUser>>();

            var userName = CreateUniqueUserName();
            var userToCreate = CreateTestUser(userName);
            Assert.True((await userManager.CreateAsync(userToCreate, "Aa123456")).Succeeded);

            var user = await userManager.FindByNameAsync(userName);
            Assert.NotNull(user);
            Assert.Equal(userName.ToUpper(), user.NormalizedUserName);
        }

        [Fact, TestPriority(2)]
        public async Task CreateRole_AssignUserToRole()
        {
            var userManager = _serviceProvider.GetService<UserManager<IdentityUser>>();
            var roleManager = _serviceProvider.GetService<RoleManager<IdentityRole>>();

            var userName = CreateUniqueUserName();
            Assert.True((await userManager.CreateAsync(CreateTestUser(userName), "Aa123456")).Succeeded, "CreateUser");
            var user = await userManager.FindByNameAsync(userName);
            Assert.NotNull(user);

            var roleName = CreateUniqueRoleName();
            Assert.True((await roleManager.CreateAsync(CreateTestRole(roleName))).Succeeded, "CreateRole");

            var role = await roleManager.FindByNameAsync(roleName);
            Assert.Equal(roleName.ToUpper(), role.NormalizedName);

            await roleManager.AddClaimAsync(role, new System.Security.Claims.Claim("system", ""));
            var roleClaims = (await roleManager.GetClaimsAsync(role)).ToList();
            Assert.True(roleClaims.Exists(t => t.Type == "system"), "Check 'system' claim exists");

            await userManager.AddToRoleAsync(user, roleName);
            Assert.True((await userManager.IsInRoleAsync(user, roleName)), "Check user in role");
            Assert.True(userManager.SupportsUserClaim, "Is SupportsUserClaim?");
            Assert.True(userManager.SupportsUserRole, "Is SupportsUserRole?");

            var userClaims = (await userManager.GetClaimsAsync(user)).ToList();

        }

        private string CreateUniqueUserName() => $"user_{UID}@test.com";
        private string CreateUniqueRoleName() => $"role_{UID}";

        private IdentityUser CreateTestUser(string userName)
        {
            return new IdentityUser
            {
                UserName = userName,
                Email = userName,
            };
        }

        private IdentityRole CreateTestRole(string roleName)
        {
            return new IdentityRole { Name = roleName };
        }
    }
}
