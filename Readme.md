# 啟動範例專案

## 使用記憶體資料庫

1. 修改 BookStore.Web\Startup.cs 設定AddDbContext為 UseInMemoryDatabase。

```
services.AddDbContext<BookStoreDbContext>(options =>
                options.UseInMemoryDatabase("memdb"));
```
2. 啟動 BookStore.Web ，系統會自動建立資料庫及所有資料表，並建立登入帳號 admin@test.com 密碼 Aa123456 的系統管理員帳號

## 使用 MSSQL

1. 修改 appsettings.json 的 BookStoreDB ConnectionStrings，改成你的資料庫環境

```
  "ConnectionStrings": {
    "BookStoreDB": "Server=.;Database=DemoTest1;User Id=sa;Password=sasa;MultipleActiveResultSets=true"
  }
```

2. 修改 BookStore.Web\Startup.cs 設定AddDbContext為 UseSqlServer。

```
services.AddDbContext<BookStoreDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("BookStoreDB")));
```

3. 啟動 BookStore.Web ，系統會自動建立資料庫及所有資料表，並建立登入帳號 admin@test.com 密碼 Aa123456 的系統管理員帳號


## DB First 指令參考

### install dotnet-ef
dotnet tool install --global dotnet-ef --version 3.1.1

### update dotnet-ef
dotnet tool update --global dotnet-ef --version 3.1.1

### migration
dotnet ef migrations add InitialCreate --startup-project BookStore.Web --project BookStore.Repository.EntityFramework --context BookStoreDbContext

### update
dotnet ef database update --startup-project BookStore.Web --project BookStore.Repository.EntityFramework --context BookStoreDbContext

### migration CreateIdentitySchema
dotnet ef migrations add CreateIdentitySchema --startup-project BookStore.Web --project BookStore.Repository.EntityFramework --context BookStoreDbContext

### update CreateIdentitySchema
dotnet ef database update CreateIdentitySchema --startup-project BookStore.Web --project BookStore.Repository.EntityFramework --context BookStoreDbContext

## Docker

### docker build
docker build . -t bookstore

### docker run
docker run --name bstore1 -p 5000:80 bookstore
