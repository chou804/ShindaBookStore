﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using Mapster;
using System.Transactions;
using System.Security.Claims;

namespace BookStore.Web.Controllers
{
    [ClaimAuthorize(SysClaims.SystemManage)]
    public class UserManageController : Controller
    {
        readonly UserManager<IdentityUser> _userManager;
        readonly RoleManager<IdentityRole> _roleManager;

        public UserManageController(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;

            SetupMappings();
        }

        public async Task<IActionResult> Index()
        {
            var userVMs = new List<UserViewModel>();

            var users = _userManager.Users;
            foreach (var user in users)
            {
                var userVM = await MapToUserViewModel(user);
                userVMs.Add(userVM);
            }

            return View(userVMs);
        }

        #region Edit
        public async Task<IActionResult> Edit(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                return RedirectToAction("Index");

            var userVM = await MapToUserViewModel(user);

            return View(userVM);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserViewModel input)
        {
            var user = await _userManager.FindByIdAsync(input.UserId);
            if (user == null)
                return RedirectToAction("Index");

            user.UserName = input.UserName;
            user.Email = input.Email;
            user.PhoneNumber = input.PhoneNumber;

            var rolesIdDb = (await _userManager.GetRolesAsync(user)).ToHashSet();
            var rolesIsChecked = input.Roles.Where(t => t.Checked).Select(t => t.RoleName).ToHashSet();

            var rolesToAdd = rolesIsChecked.Except(rolesIdDb).ToList();
            var rolesToDelete = rolesIdDb.Except(rolesIsChecked).ToList();

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                if (rolesToAdd.Count() > 0)
                    await _userManager.AddToRolesAsync(user, rolesToAdd);

                if (rolesToDelete.Count() > 0)
                    await _userManager.RemoveFromRolesAsync(user, rolesToDelete);

                await _userManager.UpdateAsync(user);

                scope.Complete();
            }

            return RedirectToAction("Index");
        }
        #endregion

        private async Task<UserViewModel> MapToUserViewModel(IdentityUser user)
        {
            var userVM = user.Adapt<UserViewModel>();
            var allRoles = _roleManager.Roles;
            var userRoleNames = await _userManager.GetRolesAsync(user);

            foreach (var r in allRoles)
            {
                if (userRoleNames.Contains(r.Name))
                    userVM.Roles.Add(new UserRoleViewModel { RoleName = r.Name, Checked = true });
                else
                    userVM.Roles.Add(new UserRoleViewModel { RoleName = r.Name });
            }

            return userVM;
        }

        #region SetupMappings
        static bool _isConfigured;
        static void SetupMappings()
        {
            if (_isConfigured)
                return;

            lock (typeof(UserManageController))
            {
                if (!_isConfigured)
                {
                    TypeAdapterConfig<IdentityUser, UserViewModel>
                        .NewConfig() //NewConfig() 如果在別處有相設同的 Mapping 設定則會清除，改用這裡的設定
                        .Map(dest => dest.UserId, src => src.Id)
                        ;

                    _isConfigured = true;
                }
            }
        }
        #endregion
    }

    #region ViewModels
    public class UserViewModel
    {
        public string UserId { get; set; }
        [Required]
        [Display(Name = "使用者名稱")]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }
        [Display(Name = "電話")]
        public string PhoneNumber { get; set; }
        [Display(Name = "角色")]
        public List<UserRoleViewModel> Roles { get; set; } = new List<UserRoleViewModel>();
    }
    public class UserRoleViewModel
    {
        public string RoleName { get; set; }
        public bool Checked { get; set; }
    }
    #endregion


}