﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BookStore.Web.Controllers
{
    [ClaimAuthorize(SysClaims.SystemManage)]
    public class RoleManageController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<HomeController> _logger;

        public RoleManageController(
            ILogger<HomeController> logger,
            RoleManager<IdentityRole> roleManager)
        {
            _logger = logger;
            _roleManager = roleManager;
        }

        public async Task<IActionResult> Index()
        {
            var roleVMs = new List<RoleViewModel>();
            var roles = _roleManager.Roles.ToList();
            foreach (var role in roles)
            {
                var roleVM = await MapToRoleViewModel(role);
                roleVMs.Add(roleVM);
            }

            return View(roleVMs);
        }

        #region Edit
        public async Task<IActionResult> Edit(string roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId);
            if (role == null)
                return RedirectToAction("Index");

            var roleVM = await MapToRoleViewModel(role);

            return View(roleVM);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(RoleViewModel input)
        {
            var role = await _roleManager.FindByIdAsync(input.RoleId);
            if (role == null)
                return RedirectToAction("Index");

            role.Name = input.RoleName;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                //全部刪除舊的 RoleClaims
                foreach (var c in await _roleManager.GetClaimsAsync(role))
                    await _roleManager.RemoveClaimAsync(role, c);

                //加入新設定的 RoleClaims
                await AddRoleClaims(input, role);

                //更新 role 資料
                await _roleManager.UpdateAsync(role);

                scope.Complete();
            }

            return RedirectToAction("Index");
        }


        #endregion

        #region Create
        public async Task<IActionResult> Create()
        {
            var emptyRole = new IdentityRole();
            var roleVM = await MapToRoleViewModel(emptyRole);

            return View(roleVM);
        }

        [HttpPost]
        public async Task<IActionResult> Create(RoleViewModel input)
        {
            var role = new IdentityRole
            {
                Id = input.RoleId,
                Name = input.RoleName
            };

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _roleManager.CreateAsync(role);
                await AddRoleClaims(input, role);

                scope.Complete();
            }

            return RedirectToAction("Index");
        }
        #endregion

        /// <summary>
        /// 加入 Role's claims
        /// </summary>
        /// <param name="roleVM"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        private async Task AddRoleClaims(RoleViewModel roleVM, IdentityRole role)
        {
            foreach (var claimVM in roleVM.RoleClaims)
            {
                if (claimVM.Enabled)
                {
                    //只有勾選 ClaimType
                    if (claimVM.ClaimValues.Count(t => t.Enabled) == 0)
                    {
                        await _roleManager.AddClaimAsync(role, new Claim(claimVM.ClaimType, ""));
                        continue;
                    }
                    //有勾選 ClaimValues
                    foreach (var claimValueVM in claimVM.ClaimValues)
                    {
                        if (claimValueVM.Enabled)
                        {
                            await _roleManager.AddClaimAsync(role, new Claim(claimVM.ClaimType, claimValueVM.ClaimValue));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 將 IdentityRole 轉換為 RoleViewModel
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        private async Task<RoleViewModel> MapToRoleViewModel(IdentityRole role)
        {
            var roleVM = new RoleViewModel
            {
                RoleId = role.Id,
                RoleName = role.Name
            };

            var roleClaims = (await _roleManager.GetClaimsAsync(role)).ToList();

            foreach (var (sysClaimType, sysClaimValues) in SysClaims.GetAll())
            {
                var claimVM = new ClaimViewModel
                {
                    ClaimType = sysClaimType,
                    ClaimValues = sysClaimValues.Select(t => new ClaimValueViewModel { ClaimValue = t }).ToList()
                };

                var claimsInRole = roleClaims.Where(t => t.Type == sysClaimType).ToList();
                if (claimsInRole.Count > 0)
                {
                    claimVM.Enabled = true;

                    var claimValues = claimsInRole.Select(t => t.Value).ToList();
                    foreach (var claimValueVM in claimVM.ClaimValues)
                    {
                        if (claimValues.Contains(claimValueVM.ClaimValue))
                            claimValueVM.Enabled = true;
                    }
                }

                roleVM.RoleClaims.Add(claimVM);
            }

            return roleVM;
        }
    }

    #region VieModels
    public class RoleViewModel
    {
        public string RoleId { get; set; }

        [Required]
        [Display(Name = "角色名稱")]
        public string RoleName { get; set; }

        public List<ClaimViewModel> RoleClaims { get; set; } = new List<ClaimViewModel>();
    }

    public class ClaimViewModel
    {
        public string ClaimType { get; set; }
        public bool Enabled { get; set; }

        public List<ClaimValueViewModel> ClaimValues { get; set; } = new List<ClaimValueViewModel>();
    }

    public class ClaimValueViewModel
    {
        public string ClaimValue { get; set; }
        public bool Enabled { get; set; }
    }
    #endregion
}