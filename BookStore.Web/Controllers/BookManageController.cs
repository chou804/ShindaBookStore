﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BookStore.Domain;
using BookStore.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Mapster;
using Microsoft.AspNetCore.Authorization;

namespace BookStore.Web.Controllers
{
    [Authorize]
    public class BookManageController : Controller
    {
        readonly BookManager _bookManager;

        public BookManageController(BookManager bookManager)
        {
            /*注入 BookManager 物件*/
            _bookManager = bookManager;
        }

        [ClaimAuthorize(SysClaims.BookManage)]
        //[Authorize(Policy = "SystemManageOnly")]
        public IActionResult Index()
        {
            /*透過 BookManager 取得所有的書籍資料*/
            var books = _bookManager.GetBooks().Adapt<List<BookViewModel>>();
            return View(books);
        }

        [HttpGet("BookManage/GetAll")]
        public IActionResult GetAll()
        {
            var books = _bookManager.GetBooks().Adapt<List<BookViewModel>>();
            return View("Index", books);
        }

        public IActionResult Search(BookQueryViewModel query)
        {
            var books = _bookManager.GetBooks()
                         .WhereIf(!string.IsNullOrWhiteSpace(query.ISBN), t => t.ISBN == query.ISBN)
                         .WhereIf(!string.IsNullOrWhiteSpace(query.BookName), t => t.BookName.Contains(query.BookName))
                         .WhereIf(!string.IsNullOrWhiteSpace(query.Author), t => t.Author.Contains(query.Author))
                         .WhereIf(query.Quantity > 0, t => t.Quantity > query.Quantity)
                         .OrderBy(t => t.BookName)
                         .ToList();

            return View(books);
        }
    }

    #region ViewModels

    public class BookViewModel
    {
        public string Id { get; set; }
        [Display(Name = "書名")]
        public string BookName { get; set; }
        [Display(Name = "ISBN")]
        public string ISBN { get; set; }
        [Display(Name = "作者")]
        public string Author { get; set; }
        [Display(Name = "銷售狀態")]
        public string SaleStatus { get; set; }
    }

    public class BookQueryViewModel
    {
        public string BookName { get; set; }
        public string ISBN { get; set; }
        public string Author { get; set; }
        public int Quantity { get; set; }
    }
    #endregion
}