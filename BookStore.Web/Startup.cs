﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookStore.Domain;
using BookStore.Domain.Services;
using BookStore.Domain.Repositories;
using BookStore.Repository.EntityFramework;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using BookStore.Web.Services;
using BookStore.Domain.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Authorization;

namespace BookStore.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BookStoreDbContext>(options =>
                options.UseInMemoryDatabase("memdb"));
                //options.UseSqlServer(Configuration.GetConnectionString("BookStoreDB")));

            /*設定 Identity，使用預設的 IdentityUser及IdentityRole 物件 */
            services.AddIdentityCore<IdentityUser>(options =>
            {
                options.SignIn.RequireConfirmedAccount = false;

                // 設定User密碼複雜度
                options.Password.RequireNonAlphanumeric = false;
            })
            .AddRoles<IdentityRole>()
            .AddEntityFrameworkStores<BookStoreDbContext>()
            .AddSignInManager()
            .AddDefaultTokenProviders();

            /*加入驗證機制*/
            services.AddAuthentication(o =>
            {
                o.DefaultScheme = IdentityConstants.ApplicationScheme;
                o.DefaultSignInScheme = IdentityConstants.ExternalScheme;
            })
            .AddIdentityCookies(o =>
            {
                /*設定 IdentityConstants.ApplicationScheme cookies*/
                o.ApplicationCookie.Configure(options =>
                {
                    options.Cookie.HttpOnly = true;
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(15);

                    options.LoginPath = "/Identity/Account/Login";
                    options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                    options.SlidingExpiration = true;
                });
            });

            /*使用 ASP.NET Core 的 policy-based authorization*/
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("BookManageOnly", policy => policy.RequireClaim("BookManage"));

            //    // 使用者必須有 ClaimType 為 SystemManage
            //    options.AddPolicy("SystemManageOnly", policy => policy.RequireClaim("SystemManage"));

            //    // 使用者必須有 ClaimType 為 SystemManage, 且須具備  Create 或 Update 的其中一個 ClaimValue
            //    options.AddPolicy("SystemManageCreateUpdate", policy => policy.RequireClaim("SystemManage", "Create", "Update"));

            //    // 使用者必須有 ClaimType 為 SystemManage, 且須具備  Create 或 Approve 的其中一個 ClaimValue
            //    options.AddPolicy("SystemManageCreateApprove", policy => policy.RequireClaim("SystemManage", "Create", "Approve"));
            //});

            /*從 appsettings.json 讀取 DomainOptions 設定值*/
            services.ConfigureDomainOptions<DomainOptions>(Configuration.GetSection("DomainOptions"));
            /*手動設定 DomainOptions*/
            //services.ConfigureDomainOptions<DomainOptions>(opt =>
            //{
            //    opt.Book.LocalBookImageBasePath = "c:/temp123";
            //});

            /*註冊 DomainLogger 讓 domain project 可以使用 ILog*/
            services.AddScoped(typeof(ILog<>), typeof(DomainLogger<>));

            /*註冊 BookManager 及相關 services*/
            //services.AddScoped<IBookRepository, EFBookRepository>();
            services.AddScoped<IImageProcessor, DummyImageProcessor>();
            services.AddScoped<IImageStorage, LocalDiskImageStorage>();
            services.AddScoped<BookManager>();

            /*註冊假的IEmailSender*/
            services.AddTransient<IEmailSender, FakeEmailSender>();

            /*加入 ClaimAuthoriztion 權限控管*/
            services.AddClaimAuthoriztion();

            services.AddControllersWithViews();
            services.AddRazorPages().AddRazorRuntimeCompilation(); /*使用預設Ideneity UI必須要加入RazorPages*/
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            //初始化資料庫
            Data.DbInitializer.Initialize(
                serviceProvider.GetRequiredService<BookStoreDbContext>(),
                serviceProvider.GetRequiredService<UserManager<IdentityUser>>(),
                serviceProvider.GetRequiredService<RoleManager<IdentityRole>>()
                );

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            /*加入UseAuthentication 放在 UseAuthorization 前面*/
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages(); /*使用預設Ideneity UI必須開啟MapRazorPages*/
            });
        }
    }
}
