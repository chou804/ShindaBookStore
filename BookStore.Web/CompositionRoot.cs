﻿using BookStore.Domain.Repositories;
using BookStore.Repository.EntityFramework;
using BookStore.Web.Repositories;
using LightInject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Web
{
	public class CompositionRoot : ICompositionRoot
	{
		public void Compose(IServiceRegistry registry)
		{
			registry.SetDefaultLifetime<PerScopeLifetime>();

			registry.Register<IBookRepository, EFBookRepository>(new PerScopeLifetime());
			registry.Decorate(typeof(IBookRepository), typeof(MemoryCacheBookRepository));
		}
	}
}
