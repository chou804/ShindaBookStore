﻿using BookStore.Domain.Services;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Web
{
    public class DomainLogger<T> : ILog<T>
    {
        readonly ILogger<T> _logger;
        public DomainLogger(ILoggerFactory factory)
        {
            _logger = factory.CreateLogger<T>();
        }

        public void Critical(string msg)
        {
            _logger.LogCritical(msg);
        }

        public void Critical(Exception exception, string msg)
        {
            _logger.LogCritical(exception, msg);
        }

        public void Debug(string msg)
        {
            _logger.LogDebug(msg);
        }

        public void Debug(Exception exception, string msg)
        {
            _logger.LogDebug(exception, msg);
        }

        public void Error(string msg)
        {
            _logger.LogError(msg);
        }

        public void Error(Exception exception, string msg)
        {
            _logger.LogError(exception, msg);
        }

        public void Info(string msg)
        {
            _logger.LogInformation(msg);
        }

        public void Info(Exception exception, string msg)
        {
            _logger.LogInformation(exception, msg);
        }

        public void Trace(string msg)
        {
            _logger.LogTrace(msg);
        }

        public void Trace(Exception exception, string msg)
        {
            _logger.LogTrace(exception, msg);
        }

        public void Warn(string msg)
        {
            _logger.LogWarning(msg);
        }

        public void Warn(Exception exception, string msg)
        {
            _logger.LogWarning(exception, msg);
        }
    }
}
