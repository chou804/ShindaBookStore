﻿using BookStore.Domain.Models;
using BookStore.Domain.Repositories;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BookStore.Web.Repositories
{
    /// <summary>
    /// Cache 範例
    /// </summary>
    public class MemoryCacheBookRepository : IBookRepository
    {
        private IMemoryCache _cache;
        private IBookRepository _bookRepository;

        public MemoryCacheBookRepository(
            IBookRepository bookRepository,
            IMemoryCache memoryCache)
        {
            _bookRepository = bookRepository;
            _cache = memoryCache;
        }

        public void Delete(Book entityToDelete)
        {
            _bookRepository.Delete(entityToDelete);
            _cache.Remove(entityToDelete.Id);
        }

        public void Delete(string uid)
        {
            _bookRepository.Delete(uid);
        }

        public IQueryable<Book> Get(Expression<Func<Book, bool>> filter = null)
        {
            return _bookRepository.Get(filter);
        }

        public Book GetByID(string uid)
        {
            if (!_cache.TryGetValue(uid, out Book cacheBook))
            {
                cacheBook = _bookRepository.GetByID(uid);
                if (cacheBook != null)
                {
                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                        .SetSlidingExpiration(TimeSpan.FromSeconds(15));

                    _cache.Set(uid, cacheBook, cacheEntryOptions);
                }
            }

            return cacheBook;
        }

        public void Insert(Book entity)
        {
            _bookRepository.Insert(entity);
        }

        public void SaveChanges()
        {
            _bookRepository.SaveChanges();
        }

        public Task SaveChangesAsync()
        {
            return _bookRepository.SaveChangesAsync();
        }

        public void Update(Book entityToUpdate)
        {
            _bookRepository.Update(entityToUpdate);
        }
    }
}
