﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Web
{
    public static class SysClaims
    {
        /*
         * 設定 system resources
         */
        public const string BookManage = "BookManage";
        public const string Audit = "AudAudititor";
        public const string SystemManage = "SystemManage";

        /*
         * 設定 system actions
         */
        public static class Values
        {
            public const string Create = "Create";
            public const string Read = "Read";
            public const string Update = "Update";
            public const string Delete = "Delete";
            public const string Approve = "Approve";
        }

        public static List<(string ClaimType, string[] ClaimValues)> GetAll()
        {
            /*
             * 設定所有的 system resources 以及預設 resource 可以被允許執行的 actions
             */

            return new List<(string ClaimType, string[] ClaimValues)>
            {
                ( SystemManage, new string[]{ Values.Create, Values.Read, Values.Update } ),
                ( BookManage, new string[]{ Values.Create, Values.Read, Values.Update, Values.Approve } ),
                ( Audit, new string[]{ } ),
            };
        }

        //public static Dictionary<string, string[]> Claims = new Dictionary<string, string[]>
        //{
        //    { SystemManage, new string[]{ } },
        //    { Audit, new string[]{ } },
        //    { BookManage, new string[]{ Values.Create, Values.Read, Values.Update, Values.Approve } },
        //};
    }
}
