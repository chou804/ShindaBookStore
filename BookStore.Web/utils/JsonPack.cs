﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BookStore.Web
{
    public class JsonPack
    {
        private static readonly JsonSerializerOptions jso = new JsonSerializerOptions { Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping };

        public static string UnsafeSerialize(object value)
        {
            return JsonSerializer.Serialize(value, jso);
        }

        public static T UnsafeDeserialize<T>(string json)
        {
            return JsonSerializer.Deserialize<T>(json, jso);
        }
    }
}
