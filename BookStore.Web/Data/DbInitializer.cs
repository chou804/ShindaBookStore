﻿using BookStore.Repository.EntityFramework;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;

namespace BookStore.Web.Data
{
    public class DbInitializer
    {

        public static void Initialize(BookStoreDbContext context, UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            context.Database.EnsureCreated();

            SetupData(userManager, roleManager).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        static async Task SetupData(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            var adminEmail = "admin@test.com";

            if (await userManager.FindByEmailAsync(adminEmail) == null)
            {
                var adminUser = new IdentityUser
                {
                    UserName = adminEmail,
                    Email = adminEmail,
                };

                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    //建立 Admin 角色
                    var adminRole = new IdentityRole("SystemAdmin");
                    await roleManager.CreateAsync(adminRole);
                    await roleManager.AddClaimAsync(adminRole, new Claim(SysClaims.SystemManage, ""));
                    await roleManager.AddClaimAsync(adminRole, new Claim(SysClaims.BookManage, ""));

                    //建立 Manager 角色
                    var managerRole = new IdentityRole("Manager");
                    await roleManager.CreateAsync(managerRole);
                    await roleManager.AddClaimAsync(managerRole, new Claim(SysClaims.BookManage, ""));

                    //建立 Auditor 角色
                    var auditorRole = new IdentityRole("Auditor");
                    await roleManager.CreateAsync(auditorRole);
                    await roleManager.AddClaimAsync(auditorRole, new Claim(SysClaims.Audit, ""));


                    //建立 admin user, 密碼 Aa123456
                    await userManager.CreateAsync(adminUser, "Aa123456");
                    await userManager.AddToRoleAsync(adminUser, adminRole.Name);

                    scope.Complete();
                }
            }
        }
    }
}
