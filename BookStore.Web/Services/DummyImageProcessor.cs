﻿using BookStore.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Web.Services
{
    public class DummyImageProcessor : IImageProcessor
    {
        public void Compress(string filePath)
        {
        }

        public void Resize(string filePath, int width, int height)
        {
        }
    }
}
