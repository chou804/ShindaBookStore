func @_BookStore.Domain.SequentialGuid.NewSequentialGuid$$() -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :34 :8) {
^entry :
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetGuidValue
// Entity from another assembly: DateTime
%0 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :38 :51) // DateTime.Now (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :38 :38) // GetGuidValue(DateTime.Now) (InvocationExpression)
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :38 :19) // new SequentialGuid(GetGuidValue(DateTime.Now)) (ObjectCreationExpression)
return %2 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :38 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.New$$() -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :41 :8) {
^entry :
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NewSequentialGuid
%0 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :43 :19) // NewSequentialGuid() (InvocationExpression)
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :43 :19) // NewSequentialGuid().ToString() (InvocationExpression)
return %1 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :43 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function GetGuidValue(none), it contains poisonous unsupported syntaxes

func @_BookStore.Domain.SequentialGuid.GetCurrentSequence$System.DateTime$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :79 :8) {
^entry (%_now : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :79 :47)
cbde.store %_now, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :79 :47)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :81 :32) // Not a variable of known type: now
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :81 :32) // now.Ticks (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :81 :44) // Not a variable of known type: SequencePeriodStart
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :81 :44) // SequencePeriodStart.Ticks (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :81 :32) // Binary expression on unsupported types now.Ticks - SequencePeriodStart.Ticks
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :82 :34) // Not a variable of known type: ticksUntilNow
%8 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :82 :25) // (decimal)ticksUntilNow (CastExpression)
%9 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :82 :50) // Not a variable of known type: TotalPeriod
%10 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :82 :50) // TotalPeriod.Ticks (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :82 :25) // Binary expression on unsupported types (decimal)ticksUntilNow / TotalPeriod.Ticks
%13 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :83 :32) // Not a variable of known type: factor
%14 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :83 :41) // Not a variable of known type: MaximumPermutations
%15 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :83 :32) // Binary expression on unsupported types factor * MaximumPermutations
%17 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :84 :35) // Not a variable of known type: resultDecimal
%18 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :84 :29) // (long)resultDecimal (CastExpression)
%20 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :85 :19) // Not a variable of known type: resultLong
return %20 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :85 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.GetGuid$long$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :89 :8) {
^entry (%_sequence : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :89 :36)
cbde.store %_sequence, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :89 :36)
br ^0

^0: // LockBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :91 :18) // Not a variable of known type: SynchronizationObject
br ^1

^1: // BinaryBranchBlock
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :93 :20) // Not a variable of known type: sequence
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :93 :32) // Not a variable of known type: _lastSequence
%4 = cbde.unknown : i1  loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :93 :20) // comparison of unknown type: sequence <= _lastSequence
cond_br %4, ^2, ^3 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :93 :20)

^2: // SimpleBlock
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :96 :31) // Not a variable of known type: _lastSequence
%6 = constant 1 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :96 :47)
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :96 :31) // Binary expression on unsupported types _lastSequence + 1
br ^3

^3: // SimpleBlock
%8 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :98 :32) // Not a variable of known type: sequence
br ^4

^4: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetSequenceBytes
%9 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :101 :49) // Not a variable of known type: sequence
%10 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :101 :32) // GetSequenceBytes(sequence) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetGuidBytes
%12 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :102 :28) // GetGuidBytes() (InvocationExpression)
%14 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :103 :29) // Not a variable of known type: guidBytes
%15 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :103 :46) // Not a variable of known type: sequenceBytes
%16 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :103 :29) // guidBytes.Concat(sequenceBytes) (InvocationExpression)
%17 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :103 :29) // guidBytes.Concat(sequenceBytes).ToArray() (InvocationExpression)
%19 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :104 :34) // Not a variable of known type: totalBytes
%20 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :104 :25) // new Guid(totalBytes) (ObjectCreationExpression)
%22 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :105 :19) // Not a variable of known type: result
return %22 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :105 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.GetSequenceBytes$long$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :108 :8) {
^entry (%_sequence : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :108 :58)
cbde.store %_sequence, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :108 :58)
br ^0

^0: // JumpBlock
// Entity from another assembly: BitConverter
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :110 :54) // Not a variable of known type: sequence
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :110 :32) // BitConverter.GetBytes(sequence) (InvocationExpression)
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :111 :42) // Not a variable of known type: sequenceBytes
%5 = constant 6 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :111 :72)
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :111 :67) // byte[NumberOfSequenceBytes] (ArrayType)
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :111 :63) // new byte[NumberOfSequenceBytes] (ArrayCreationExpression)
%8 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :111 :42) // sequenceBytes.Concat(new byte[NumberOfSequenceBytes]) (InvocationExpression)
%10 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :112 :25) // Not a variable of known type: sequenceBytesLongEnough
%11 = constant 6 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :112 :54)
%12 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :112 :25) // sequenceBytesLongEnough.Take(NumberOfSequenceBytes) (InvocationExpression)
%13 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :112 :25) // sequenceBytesLongEnough.Take(NumberOfSequenceBytes).Reverse() (InvocationExpression)
%15 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :113 :19) // Not a variable of known type: result
return %15 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :113 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.GetGuidBytes$$() -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :116 :8) {
^entry :
br ^0

^0: // JumpBlock
// Entity from another assembly: Guid
%0 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :118 :19) // Guid.NewGuid() (InvocationExpression)
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :118 :19) // Guid.NewGuid().ToByteArray() (InvocationExpression)
%2 = constant 10 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :118 :53)
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :118 :19) // Guid.NewGuid().ToByteArray().Take(10) (InvocationExpression)
return %3 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :118 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.GetCreatedDateTime$System.Guid$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :133 :8) {
^entry (%_value : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :133 :52)
cbde.store %_value, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :133 :52)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetSequenceLongBytes
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :135 :53) // Not a variable of known type: value
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :135 :32) // GetSequenceLongBytes(value) (InvocationExpression)
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :135 :32) // GetSequenceLongBytes(value).ToArray() (InvocationExpression)
// Entity from another assembly: BitConverter
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :136 :52) // Not a variable of known type: sequenceBytes
%6 = constant 0 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :136 :67)
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :136 :31) // BitConverter.ToInt64(sequenceBytes, 0) (InvocationExpression)
%9 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :137 :43) // Not a variable of known type: sequenceLong
%10 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :137 :34) // (decimal)sequenceLong (CastExpression)
%12 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :138 :25) // Not a variable of known type: sequenceDecimal
%13 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :138 :43) // Not a variable of known type: MaximumPermutations
%14 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :138 :25) // Binary expression on unsupported types sequenceDecimal / MaximumPermutations
%16 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :139 :32) // Not a variable of known type: factor
%17 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :139 :41) // Not a variable of known type: TotalPeriod
%18 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :139 :41) // TotalPeriod.Ticks (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :139 :32) // Binary expression on unsupported types factor * TotalPeriod.Ticks
%21 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :140 :34) // Not a variable of known type: ticksUntilNow
%22 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :140 :50) // Not a variable of known type: SequencePeriodStart
%23 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :140 :50) // SequencePeriodStart.Ticks (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :140 :34) // Binary expression on unsupported types ticksUntilNow + SequencePeriodStart.Ticks
%26 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :141 :33) // Not a variable of known type: nowTicksDecimal
%27 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :141 :27) // (long)nowTicksDecimal (CastExpression)
%29 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :142 :38) // Not a variable of known type: nowTicks
%30 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :142 :25) // new DateTime(nowTicks) (ObjectCreationExpression)
%32 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :143 :19) // Not a variable of known type: result
return %32 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :143 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.GetSequenceLongBytes$System.Guid$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :146 :8) {
^entry (%_value : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :146 :62)
cbde.store %_value, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :146 :62)
br ^0

^0: // JumpBlock
%1 = constant 8 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :148 :44)
%2 = cbde.alloca i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :148 :22) // numberOfBytesOfLong
cbde.store %1, %2 : memref<i32> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :148 :22)
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :149 :32) // Not a variable of known type: value
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :149 :32) // value.ToByteArray() (InvocationExpression)
%5 = constant 10 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :149 :57)
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :149 :32) // value.ToByteArray().Skip(10) (InvocationExpression)
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :149 :32) // value.ToByteArray().Skip(10).Reverse() (InvocationExpression)
%8 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :149 :32) // value.ToByteArray().Skip(10).Reverse().ToArray() (InvocationExpression)
%10 = cbde.load %2 : memref<i32> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :150 :39)
%11 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :150 :61) // Not a variable of known type: sequenceBytes
%12 = cbde.unknown : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :150 :61) // sequenceBytes.Length (SimpleMemberAccessExpression)
%13 = subi %10, %12 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :150 :39)
%14 = cbde.alloca i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :150 :16) // additionalBytesCount
cbde.store %13, %14 : memref<i32> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :150 :16)
%15 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :151 :19) // Not a variable of known type: sequenceBytes
%16 = cbde.load %14 : memref<i32> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :151 :49)
%17 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :151 :44) // byte[additionalBytesCount] (ArrayType)
%18 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :151 :40) // new byte[additionalBytesCount] (ArrayCreationExpression)
%19 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :151 :19) // sequenceBytes.Concat(new byte[additionalBytesCount]) (InvocationExpression)
return %19 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :151 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.CompareTo$object$(none) -> i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :256 :8) {
^entry (%_obj : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :256 :29)
cbde.store %_obj, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :256 :29)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :258 :16) // Not a variable of known type: obj
%2 = cbde.unknown : i1 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :258 :16) // obj is SequentialGuid (IsExpression)
cond_br %2, ^1, ^2 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :258 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CompareTo
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :260 :49) // Not a variable of known type: obj
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :260 :33) // (SequentialGuid)obj (CastExpression)
%5 = cbde.unknown : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :260 :23) // CompareTo((SequentialGuid)obj) (InvocationExpression)
return %5 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :260 :16)

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :262 :16) // Not a variable of known type: obj
%7 = cbde.unknown : i1 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :262 :16) // obj is Guid (IsExpression)
cond_br %7, ^3, ^4 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :262 :16)

^3: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CompareTo
%8 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :264 :39) // Not a variable of known type: obj
%9 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :264 :33) // (Guid)obj (CastExpression)
%10 = cbde.unknown : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :264 :23) // CompareTo((Guid)obj) (InvocationExpression)
return %10 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :264 :16)

^4: // JumpBlock
%11 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :266 :40) // "Parameter is not of the rigt type" (StringLiteralExpression)
%12 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :266 :18) // new ArgumentException("Parameter is not of the rigt type") (ObjectCreationExpression)
cbde.throw %12 :  none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :266 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.CompareTo$BookStore.Domain.SequentialGuid$(none) -> i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :269 :8) {
^entry (%_other : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :269 :29)
cbde.store %_other, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :269 :29)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CompareTo
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :271 :29) // Not a variable of known type: other
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :271 :29) // other._guidValue (SimpleMemberAccessExpression)
%3 = cbde.unknown : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :271 :19) // CompareTo(other._guidValue) (InvocationExpression)
return %3 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :271 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.CompareTo$System.Guid$(none) -> i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :274 :8) {
^entry (%_other : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :274 :29)
cbde.store %_other, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :274 :29)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CompareImplementation
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :276 :41) // Not a variable of known type: _guidValue
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :276 :53) // Not a variable of known type: other
%3 = cbde.unknown : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :276 :19) // CompareImplementation(_guidValue, other) (InvocationExpression)
return %3 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :276 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function CompareImplementation(none, none), it contains poisonous unsupported syntaxes

// Skipping function Equals(none), it contains poisonous unsupported syntaxes

func @_BookStore.Domain.SequentialGuid.Equals$BookStore.Domain.SequentialGuid$(none) -> i1 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :305 :8) {
^entry (%_other : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :305 :27)
cbde.store %_other, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :305 :27)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CompareTo
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :307 :29) // Not a variable of known type: other
%2 = cbde.unknown : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :307 :19) // CompareTo(other) (InvocationExpression)
%3 = constant 0 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :307 :39)
%4 = cmpi "eq", %2, %3 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :307 :19)
return %4 : i1 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :307 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.Equals$System.Guid$(none) -> i1 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :310 :8) {
^entry (%_other : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :310 :27)
cbde.store %_other, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :310 :27)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CompareTo
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :312 :29) // Not a variable of known type: other
%2 = cbde.unknown : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :312 :19) // CompareTo(other) (InvocationExpression)
%3 = constant 0 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :312 :39)
%4 = cmpi "eq", %2, %3 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :312 :19)
return %4 : i1 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :312 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.GetHashCode$$() -> i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :315 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :317 :19) // Not a variable of known type: _guidValue
%1 = cbde.unknown : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :317 :19) // _guidValue.GetHashCode() (InvocationExpression)
return %1 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :317 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.ToString$$() -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :338 :8) {
^entry :
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Round
%0 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :340 :47) // Not a variable of known type: CreatedDateTime
// Entity from another assembly: TimeSpan
%1 = constant 1 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :340 :90)
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :340 :64) // TimeSpan.FromMilliseconds(1) (InvocationExpression)
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :340 :41) // Round(CreatedDateTime, TimeSpan.FromMilliseconds(1)) (InvocationExpression)
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :343 :19) // string (PredefinedType)
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :343 :33) // "{0}" (StringLiteralExpression)
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :344 :33) // Not a variable of known type: _guidValue
%8 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :343 :19) // string.Format("{0}",                                   _guidValue) (InvocationExpression)
return %8 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :343 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.SequentialGuid.Round$System.DateTime.System.TimeSpan$(none, none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :347 :8) {
^entry (%_dateTime : none, %_interval : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :347 :38)
cbde.store %_dateTime, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :347 :38)
%1 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :347 :57)
cbde.store %_interval, %1 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :347 :57)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :349 :37) // Not a variable of known type: interval
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :349 :37) // interval.Ticks (SimpleMemberAccessExpression)
%4 = constant 1 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :349 :54)
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :349 :37) // Binary expression on unsupported types interval.Ticks + 1
%6 = constant 1 : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :349 :60)
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :349 :36) // Binary expression on unsupported types (interval.Ticks + 1) >> 1
%9 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :351 :19) // Not a variable of known type: dateTime
%10 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :351 :37) // Not a variable of known type: halfIntervalTicks
%11 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :352 :21) // Not a variable of known type: dateTime
%12 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :352 :21) // dateTime.Ticks (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :352 :38) // Not a variable of known type: halfIntervalTicks
%14 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :352 :21) // Binary expression on unsupported types dateTime.Ticks + halfIntervalTicks
%15 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :352 :59) // Not a variable of known type: interval
%16 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :352 :59) // interval.Ticks (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :352 :20) // Binary expression on unsupported types (dateTime.Ticks + halfIntervalTicks) % interval.Ticks
%18 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :351 :37) // Binary expression on unsupported types halfIntervalTicks -                     ((dateTime.Ticks + halfIntervalTicks) % interval.Ticks)
%19 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :351 :19) // dateTime.AddTicks(halfIntervalTicks -                     ((dateTime.Ticks + halfIntervalTicks) % interval.Ticks)) (InvocationExpression)
return %19 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\SequentialGuid.cs" :351 :12)

^1: // ExitBlock
cbde.unreachable

}
