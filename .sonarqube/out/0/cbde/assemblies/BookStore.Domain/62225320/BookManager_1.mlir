func @_BookStore.Domain.BookManager.CreateBook$BookStore.Domain.Models.Book$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :32 :8) {
^entry (%_book : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :32 :31)
cbde.store %_book, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :32 :31)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Ensure
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :34 :36) // Not a variable of known type: book
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :34 :42) // nameof(book) (InvocationExpression)
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :34 :12) // Ensure.ArgumentNotEmpty(book, nameof(book)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_BookStore.Domain.BookManager.CreateBookImage$string$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :42 :8) {
^entry (%_sourceImageFilePath : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :42 :41)
cbde.store %_sourceImageFilePath, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :42 :41)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :44 :12) // Not a variable of known type: _imageProcessor
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :44 :37) // Not a variable of known type: sourceImageFilePath
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :44 :12) // _imageProcessor.Compress(sourceImageFilePath) (InvocationExpression)
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :46 :22) // new BookImage              {                  Id = SequentialGuid.New(),                  Enable = true,              } (ObjectCreationExpression)
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :48 :21) // Not a variable of known type: SequentialGuid
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :48 :21) // SequentialGuid.New() (InvocationExpression)
%7 = constant 1 : i1 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :49 :25) // true
%9 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :51 :12) // Not a variable of known type: _imageStorage
%10 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :51 :40) // Not a variable of known type: sourceImageFilePath
%11 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :51 :61) // Not a variable of known type: img
%12 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :51 :12) // _imageStorage.SaveBookImage(sourceImageFilePath, img) (InvocationExpression)
%13 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :53 :19) // Not a variable of known type: img
return %13 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :53 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.BookManager.GetBooks$System.Linq.Expressions.Expression$System.Func$BookStore.Domain.Models.Book.bool$$$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :56 :8) {
^entry (%_filter : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :56 :41)
cbde.store %_filter, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :56 :41)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :58 :19) // Not a variable of known type: _bookRepository
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :58 :39) // Not a variable of known type: filter
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :58 :19) // _bookRepository.Get(filter) (InvocationExpression)
return %3 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :58 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.BookManager.GetBookById$string$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :61 :8) {
^entry (%_id : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :61 :32)
cbde.store %_id, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :61 :32)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :63 :19) // Not a variable of known type: _bookRepository
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :63 :43) // Not a variable of known type: id
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :63 :19) // _bookRepository.GetByID(id) (InvocationExpression)
return %3 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :63 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Domain.BookManager.UpdateBook$BookStore.Domain.Models.Book$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :66 :8) {
^entry (%_book : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :66 :31)
cbde.store %_book, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :66 :31)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :68 :12) // Not a variable of known type: _bookRepository
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :68 :35) // Not a variable of known type: book
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :68 :12) // _bookRepository.Update(book) (InvocationExpression)
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :69 :12) // Not a variable of known type: _bookRepository
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\BookManager.cs" :69 :12) // _bookRepository.SaveChanges() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
