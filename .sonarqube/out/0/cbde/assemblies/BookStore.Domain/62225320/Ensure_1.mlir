func @_BookStore.Domain.Ensure.ThrowIf$bool.string.System.Exception$(i1, none, none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :8 :8) {
^entry (%_condition : i1, %_msg : none, %_ex : none):
%0 = cbde.alloca i1 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :8 :35)
cbde.store %_condition, %0 : memref<i1> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :8 :35)
%1 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :8 :51)
cbde.store %_msg, %1 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :8 :51)
%2 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :8 :63)
cbde.store %_ex, %2 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :8 :63)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.load %0 : memref<i1> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :10 :16)
cond_br %3, ^1, ^2 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :10 :16)

^1: // BinaryBranchBlock
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :12 :20) // Not a variable of known type: ex
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :12 :26) // null (NullLiteralExpression)
%6 = cbde.unknown : i1  loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :12 :20) // comparison of unknown type: ex != null
cond_br %6, ^3, ^4 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :12 :20)

^3: // JumpBlock
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :13 :40) // Not a variable of known type: msg
%8 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :13 :45) // Not a variable of known type: ex
%9 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :13 :26) // new Exception(msg, ex) (ObjectCreationExpression)
cbde.throw %9 :  none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :13 :20)

^4: // JumpBlock
%10 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :15 :40) // Not a variable of known type: msg
%11 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :15 :26) // new Exception(msg) (ObjectCreationExpression)
cbde.throw %11 :  none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :15 :20)

^2: // ExitBlock
return

}
// Skipping function ArgumentNotEmpty(none, none), it contains poisonous unsupported syntaxes

func @_BookStore.Domain.Ensure.ArgumentNotNull$object$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :40 :8) {
^entry (%_arg : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :40 :43)
cbde.store %_arg, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :40 :43)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :42 :16) // Not a variable of known type: arg
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :42 :23) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :42 :16) // comparison of unknown type: arg == null
cond_br %3, ^1, ^2 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :42 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :44 :22) // new ArgumentNullException() (ObjectCreationExpression)
cbde.throw %4 :  none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :44 :16)

^2: // ExitBlock
return

}
func @_BookStore.Domain.Ensure.NotEmpty$string.string.System.Exception$(none, none, none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :48 :8) {
^entry (%_str : none, %_msg : none, %_ex : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :48 :36)
cbde.store %_str, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :48 :36)
%1 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :48 :48)
cbde.store %_msg, %1 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :48 :48)
%2 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :48 :60)
cbde.store %_ex, %2 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :48 :60)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :50 :16) // string (PredefinedType)
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :50 :42) // Not a variable of known type: str
%5 = cbde.unknown : i1 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :50 :16) // string.IsNullOrWhiteSpace(str) (InvocationExpression)
cond_br %5, ^1, ^2 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :50 :16)

^1: // BinaryBranchBlock
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :52 :20) // Not a variable of known type: ex
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :52 :26) // null (NullLiteralExpression)
%8 = cbde.unknown : i1  loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :52 :20) // comparison of unknown type: ex != null
cond_br %8, ^3, ^4 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :52 :20)

^3: // JumpBlock
%9 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :53 :40) // Not a variable of known type: msg
%10 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :53 :45) // Not a variable of known type: ex
%11 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :53 :26) // new Exception(msg, ex) (ObjectCreationExpression)
cbde.throw %11 :  none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :53 :20)

^4: // JumpBlock
%12 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :55 :40) // Not a variable of known type: msg
%13 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :55 :26) // new Exception(msg) (ObjectCreationExpression)
cbde.throw %13 :  none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Domain\\utils\\Ensure.cs" :55 :20)

^2: // ExitBlock
return

}
