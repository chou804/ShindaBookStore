func @_BookStore.Repository.EntityFramework.EFBaseRepository$TEntity.TKey$.Delete$TEntity$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :24 :8) {
^entry (%_entityToDelete : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :24 :27)
cbde.store %_entityToDelete, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :24 :27)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :26 :16) // Not a variable of known type: entityToDelete
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :26 :34) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :26 :16) // comparison of unknown type: entityToDelete == null
cond_br %3, ^1, ^2 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :26 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :26 :72) // nameof(entityToDelete) (InvocationExpression)
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :26 :46) // new ArgumentNullException(nameof(entityToDelete)) (ObjectCreationExpression)
cbde.throw %5 :  none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :26 :40)

^2: // SimpleBlock
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :28 :12) // Not a variable of known type: _dbSet
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :28 :26) // Not a variable of known type: entityToDelete
%8 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :28 :12) // _dbSet.Remove(entityToDelete) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_BookStore.Repository.EntityFramework.EFBaseRepository$TEntity.TKey$.Delete$TKey$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :31 :8) {
^entry (%_uid : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :31 :27)
cbde.store %_uid, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :31 :27)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetByID
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :33 :41) // Not a variable of known type: uid
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :33 :33) // GetByID(uid) (InvocationExpression)
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :34 :12) // Not a variable of known type: _dbSet
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :34 :26) // Not a variable of known type: entityToDelete
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :34 :12) // _dbSet.Remove(entityToDelete) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_BookStore.Repository.EntityFramework.EFBaseRepository$TEntity.TKey$.Get$System.Linq.Expressions.Expression$System.Func$TEntity.bool$$$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :37 :8) {
^entry (%_filter : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :37 :39)
cbde.store %_filter, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :37 :39)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :39 :16) // Not a variable of known type: filter
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :39 :26) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :39 :16) // comparison of unknown type: filter == null
cond_br %3, ^1, ^2 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :39 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :40 :23) // Not a variable of known type: _dbSet
return %4 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :40 :16)

^2: // JumpBlock
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :42 :23) // Not a variable of known type: _dbSet
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :42 :36) // Not a variable of known type: filter
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :42 :23) // _dbSet.Where(filter) (InvocationExpression)
return %7 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :42 :16)

^3: // ExitBlock
cbde.unreachable

}
func @_BookStore.Repository.EntityFramework.EFBaseRepository$TEntity.TKey$.GetByID$TKey$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :45 :8) {
^entry (%_uid : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :45 :31)
cbde.store %_uid, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :45 :31)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :47 :19) // Not a variable of known type: _dbSet
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :47 :31) // Not a variable of known type: uid
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :47 :19) // _dbSet.Find(uid) (InvocationExpression)
return %3 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :47 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Repository.EntityFramework.EFBaseRepository$TEntity.TKey$.Insert$TEntity$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :50 :8) {
^entry (%_entity : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :50 :27)
cbde.store %_entity, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :50 :27)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :52 :16) // Not a variable of known type: entity
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :52 :26) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :52 :16) // comparison of unknown type: entity == null
cond_br %3, ^1, ^2 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :52 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :52 :64) // nameof(entity) (InvocationExpression)
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :52 :38) // new ArgumentNullException(nameof(entity)) (ObjectCreationExpression)
cbde.throw %5 :  none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :52 :32)

^2: // SimpleBlock
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :54 :12) // Not a variable of known type: _dbSet
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :54 :23) // Not a variable of known type: entity
%8 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :54 :12) // _dbSet.Add(entity) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_BookStore.Repository.EntityFramework.EFBaseRepository$TEntity.TKey$.Update$TEntity$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :57 :8) {
^entry (%_entityToUpdate : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :57 :27)
cbde.store %_entityToUpdate, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :57 :27)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :59 :16) // Not a variable of known type: entityToUpdate
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :59 :34) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :59 :16) // comparison of unknown type: entityToUpdate == null
cond_br %3, ^1, ^2 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :59 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :59 :72) // nameof(entityToUpdate) (InvocationExpression)
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :59 :46) // new ArgumentNullException(nameof(entityToUpdate)) (ObjectCreationExpression)
cbde.throw %5 :  none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :59 :40)

^2: // SimpleBlock
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :61 :12) // Not a variable of known type: _dbSet
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :61 :26) // Not a variable of known type: entityToUpdate
%8 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :61 :12) // _dbSet.Update(entityToUpdate) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_BookStore.Repository.EntityFramework.EFBaseRepository$TEntity.TKey$.SaveChanges$$() -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :64 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :66 :12) // Not a variable of known type: _context
%1 = cbde.unknown : i32 loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :66 :12) // _context.SaveChanges() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_BookStore.Repository.EntityFramework.EFBaseRepository$TEntity.TKey$.SaveChangesAsync$$() -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :69 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :71 :19) // Not a variable of known type: _context
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :71 :19) // _context.SaveChangesAsync() (InvocationExpression)
return %1 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Repository.EntityFramework\\EFBaseRepository.cs" :71 :12)

^1: // ExitBlock
cbde.unreachable

}
