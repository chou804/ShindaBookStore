Ēn
DC:\git_troy\ShindaBookStore\BookStore.API\ClaimAuthorizeAttribute.cs
	namespace 	
	BookStore
 
. 
API 
{ 
public 

class #
ClaimAuthorizeAttribute (
:) *
AuthorizeAttribute+ =
{ 
const 
string 
POLICY_PREFIX "
=# $
$str% -
;- .
readonly 
string 
_type 
; 
readonly 
string 
[ 
] 
_values !
;! "
public #
ClaimAuthorizeAttribute &
(& '
string' -
	claimType. 7
,7 8
params9 ?
string@ F
[F G
]G H
claimValuesI T
)T U
{ 	
_type 
= 
	claimType 
; 
_values 
= 
claimValues !
;! "
if 
( 
claimValues 
. 
Count !
(! "
)" #
>$ %
$num& '
)' (
{ 
Policy 
= 
$" 
{ 
POLICY_PREFIX )
}) *
<* +
{+ ,
_type, 1
}1 2
>2 3
{3 4
string4 :
.: ;
Join; ?
(? @
$str@ C
,C D
_valuesE L
)L M
}M N
"N O
;O P
} 
else 
{   
Policy!! 
=!! 
$"!! 
{!! 
POLICY_PREFIX!! )
}!!) *
<!!* +
{!!+ ,
_type!!, 1
}!!1 2
>!!2 3
"!!3 4
;!!4 5
}"" 
}## 	
}$$ 
public(( 

class(( 
ClaimRequirement(( !
:((" #%
IAuthorizationRequirement(($ =
{)) 
public** 
string** 
	ClaimType** 
{**  !
get**" %
;**% &
}**' (
public++ 
string++ 
[++ 
]++ 
ClaimValues++ #
{++$ %
get++& )
;++) *
}+++ ,
public-- 
ClaimRequirement-- 
(--  
string--  &
	claimType--' 0
,--0 1
params--2 8
string--9 ?
[--? @
]--@ A
claimValues--B M
)--M N
{.. 	
	ClaimType// 
=// 
	claimType// !
;//! "
ClaimValues00 
=00 
claimValues00 %
;00% &
}11 	
}22 
public66 

class66 
ClaimHandler66 
:66  
AuthorizationHandler66  4
<664 5
ClaimRequirement665 E
>66E F
{77 
	protected88 
override88 
Task88 "
HandleRequirementAsync88  6
(886 7'
AuthorizationHandlerContext887 R
context88S Z
,88Z [
ClaimRequirement88\ l
requirement88m x
)88x y
{99 	
if:: 
(:: 
!:: 
context:: 
.:: 
User:: 
.:: 
Identity:: &
.::& '
IsAuthenticated::' 6
)::6 7
{;; 
return<< 
Task<< 
.<< 
CompletedTask<< )
;<<) *
}== 
if@@ 
(@@ 
context@@ 
.@@ 
User@@ 
.@@ 
HasClaim@@ %
(@@% &
t@@& '
=>@@( *
t@@+ ,
.@@, -
Type@@- 1
==@@2 4
$str@@5 =
&&@@> @
t@@A B
.@@B C
Value@@C H
==@@I K
$str@@L R
)@@R S
)@@S T
{AA 
contextBB 
.BB 
SucceedBB 
(BB  
requirementBB  +
)BB+ ,
;BB, -
returnCC 
TaskCC 
.CC 
CompletedTaskCC )
;CC) *
}DD 
varFF 
claimValuesFF 
=FF 
requirementFF )
.FF) *
ClaimValuesFF* 5
.FF5 6
WhereFF6 ;
(FF; <
sFF< =
=>FF> @
!FFA B
stringFFB H
.FFH I
IsNullOrWhiteSpaceFFI [
(FF[ \
sFF\ ]
)FF] ^
)FF^ _
?FF_ `
.FF` a
DistinctFFa i
(FFi j
)FFj k
;FFk l
ifGG 
(GG 
claimValuesGG 
?GG 
.GG 
CountGG "
(GG" #
)GG# $
>GG% &
$numGG' (
)GG( )
{HH 
ifII 
(II 
contextII 
.II 
UserII  
.II  !
HasClaimII! )
(II) *
tII* +
=>II, .
tII/ 0
.II0 1
TypeII1 5
==II6 8
requirementII9 D
.IID E
	ClaimTypeIIE N
&&IIO Q
claimValuesIIR ]
.II] ^
ContainsII^ f
(IIf g
tIIg h
.IIh i
ValueIIi n
)IIn o
)IIo p
)IIp q
{JJ 
contextKK 
.KK 
SucceedKK #
(KK# $
requirementKK$ /
)KK/ 0
;KK0 1
}LL 
}MM 
elseNN 
{OO 
ifPP 
(PP 
contextPP 
.PP 
UserPP  
.PP  !
HasClaimPP! )
(PP) *
tPP* +
=>PP, .
tPP/ 0
.PP0 1
TypePP1 5
==PP6 8
requirementPP9 D
.PPD E
	ClaimTypePPE N
)PPN O
)PPO P
{QQ 
contextRR 
.RR 
SucceedRR #
(RR# $
requirementRR$ /
)RR/ 0
;RR0 1
}SS 
}TT 
returnVV 
TaskVV 
.VV 
CompletedTaskVV %
;VV% &
}WW 	
}XX 
public\\ 

class\\ 
ClaimPolicyProvider\\ $
:\\% &(
IAuthorizationPolicyProvider\\' C
{]] 
static__  
ConcurrentDictionary__ #
<__# $
string__$ *
,__* +
AuthorizationPolicy__, ?
>__? @

policyDict__A K
=__L M
new__N Q 
ConcurrentDictionary__R f
<__f g
string__g m
,__m n 
AuthorizationPolicy	__o 
>
__ 
(
__ 
)
__ 
;
__ 
constaa 
stringaa 
CLAIM_POLICY_PREFIXaa (
=aa) *
$straa+ 3
;aa3 4
staticbb 
readonlybb 
Regexbb 
rxbb  
=bb! "
newbb# &
Regexbb' ,
(bb, -
$strbb- S
,bbS T
RegexOptionsbbU a
.bba b
Compiledbbb j
)bbj k
;bbk l
privatedd 
readonlydd 
stringdd 
[dd  
]dd  !"
_authenticationSchemesdd" 8
;dd8 9
privateee .
"DefaultAuthorizationPolicyProvideree 2!
_backupPolicyProvideree3 H
{eeI J
geteeK N
;eeN O
}eeP Q
publicff 
ClaimPolicyProviderff "
(ff" #
IOptionsgg 
<gg  
AuthorizationOptionsgg )
>gg) * 
authorizationoptionsgg+ ?
,gg? @
IOptionshh 
<hh !
ClaimAuthorizeOptionshh *
>hh* +!
claimAuthorizeoptionshh, A
)hhA B
{ii 	"
_authenticationSchemesjj "
=jj# $!
claimAuthorizeoptionsjj% :
.jj: ;
Valuejj; @
.jj@ A!
AuthenticationSchemesjjA V
;jjV W!
_backupPolicyProviderkk !
=kk" #
newkk$ '.
"DefaultAuthorizationPolicyProviderkk( J
(kkJ K 
authorizationoptionskkK _
)kk_ `
;kk` a
}ll 	
publicoo 
Taskoo 
<oo 
AuthorizationPolicyoo '
>oo' (!
GetDefaultPolicyAsyncoo) >
(oo> ?
)oo? @
=>ooA C
Taskpp 
.pp 

FromResultpp 
(pp 
newpp &
AuthorizationPolicyBuilderpp  :
(pp: ;"
_authenticationSchemespp; Q
)ppQ R
.ppR S$
RequireAuthenticatedUserppS k
(ppk l
)ppl m
.ppm n
Buildppn s
(pps t
)ppt u
)ppu v
;ppv w
publicrr 
Taskrr 
<rr 
AuthorizationPolicyrr '
>rr' ("
GetFallbackPolicyAsyncrr) ?
(rr? @
)rr@ A
=>rrB D
Taskss 
.ss 

FromResultss 
<ss 
AuthorizationPolicyss /
>ss/ 0
(ss0 1
nullss1 5
)ss5 6
;ss6 7
publicuu 
Taskuu 
<uu 
AuthorizationPolicyuu '
>uu' (
GetPolicyAsyncuu) 7
(uu7 8
stringuu8 >

policyNameuu? I
)uuI J
{vv 	
ifww 
(ww 

policyNameww 
.ww 

StartsWithww %
(ww% &
CLAIM_POLICY_PREFIXww& 9
,ww9 :
StringComparisonww; K
.wwK L
OrdinalIgnoreCasewwL ]
)ww] ^
)ww^ _
{xx 
ifyy 
(yy 
!yy 

policyDictyy 
.yy  
TryGetValueyy  +
(yy+ ,

policyNameyy, 6
,yy6 7
outyy8 ;
AuthorizationPolicyyy< O
authorizationPolicyyyP c
)yyc d
)yyd e
{zz 
if{{ 
({{ 
rx{{ 
.{{ 
IsMatch{{ "
({{" #

policyName{{# -
){{- .
){{. /
{|| 
var}} 
	claimType}} %
=}}& '
rx}}( *
.}}* +
Match}}+ 0
(}}0 1

policyName}}1 ;
)}}; <
.}}< =
Groups}}= C
[}}C D
$str}}D J
]}}J K
.}}K L
Value}}L Q
;}}Q R
var~~ 
claimValues~~ '
=~~( )
rx~~* ,
.~~, -
Match~~- 2
(~~2 3

policyName~~3 =
)~~= >
.~~> ?
Groups~~? E
[~~E F
$str~~F N
]~~N O
.~~O P
Value~~P U
.~~U V
Split~~V [
(~~[ \
$str~~\ _
)~~_ `
;~~` a
var 
policy "
=# $
new% (&
AuthorizationPolicyBuilder) C
(C D"
_authenticationSchemesD Z
)Z [
.[ \$
RequireAuthenticatedUser\ t
(t u
)u v
;v w
policy
 
.
 
AddRequirements
 .
(
. /
new
/ 2
ClaimRequirement
3 C
(
C D
	claimType
D M
,
M N
claimValues
O Z
)
Z [
)
[ \
;
\ ]!
authorizationPolicy
 +
=
, -
policy
. 4
.
4 5
Build
5 :
(
: ;
)
; <
;
< =

policyDict
 "
.
" #
TryAdd
# )
(
) *

policyName
* 4
,
4 5!
authorizationPolicy
6 I
)
I J
;
J K
}
 
}
 
return
 
Task
 
.
 

FromResult
 &
(
& '!
authorizationPolicy
' :
)
: ;
;
; <
}
 
return
 #
_backupPolicyProvider
 (
.
( )
GetPolicyAsync
) 7
(
7 8

policyName
8 B
)
B C
;
C D
}
 	
}
 
public
 

static
 
class
 6
(ClaimAuthorizeServiceCollectionExtension
 @
{
 
public
 
static
 
void
 "
AddClaimAuthoriztion
 /
(
/ 0
this
0 4 
IServiceCollection
5 G
services
H P
,
P Q
params
R X
string
Y _
[
_ `
]
` a#
authenticationSchemes
b w
)
w x
{
 	
if
 
(
 #
authenticationSchemes
 %
.
% &
Length
& ,
>
- .
$num
/ 0
)
0 1
{
 
services
 
.
 
	Configure
 "
<
" ##
ClaimAuthorizeOptions
# 8
>
8 9
(
9 :
opt
: =
=>
> @
opt
A D
.
D E#
AuthenticationSchemes
E Z
=
[ \#
authenticationSchemes
] r
)
r s
;
s t
}
 
services
 
.
 
AddSingleton
 !
<
! "*
IAuthorizationPolicyProvider
" >
,
> ?!
ClaimPolicyProvider
@ S
>
S T
(
T U
)
U V
;
V W
services
 
.
 
AddSingleton
 !
<
! "#
IAuthorizationHandler
" 7
,
7 8
ClaimHandler
9 E
>
E F
(
F G
)
G H
;
H I
}
   	
}
ĄĄ 
public
ĪĪ 

class
ĪĪ #
ClaimAuthorizeOptions
ĪĪ &
{
ĨĨ 
public
ĶĶ 
string
ĶĶ 
[
ĶĶ 
]
ĶĶ #
AuthenticationSchemes
ĶĶ -
{
ĶĶ. /
get
ĶĶ0 3
;
ĶĶ3 4
set
ĶĶ5 8
;
ĶĶ8 9
}
ĶĶ: ;
=
ĶĶ< =
new
ĶĶ> A
string
ĶĶB H
[
ĶĶH I
]
ĶĶI J
{
ĶĶK L
JwtBearerDefaults
ĶĶM ^
.
ĶĶ^ _"
AuthenticationScheme
ĶĶ_ s
}
ĶĶt u
;
ĶĶu v
}
§§ 
}ĻĻ ģE
GC:\git_troy\ShindaBookStore\BookStore.API\Controllers\UserController.cs
	namespace 	
	BookStore
 
. 
API 
. 
Controllers #
{ 
[ 
	Authorize 
] 
[ 
Route 

(
 
$str 
) 
] 
[ 
ApiController 
] 
public 

class 
UserController 
:  !
ControllerBase" 0
{ 
readonly 
UserManager 
< 
IdentityUser )
>) *
_userManager+ 7
;7 8
readonly 
RoleManager 
< 
IdentityRole )
>) *
_roleManager+ 7
;7 8
readonly 
string 

JWT_SECRET "
;" #
public 
UserController 
( 
IConfiguration 
configuration (
,( )
UserManager 
< 
IdentityUser $
>$ %
userManager& 1
,1 2
RoleManager 
< 
IdentityRole $
>$ %
roleManager& 1
) 
{   	
_userManager!! 
=!! 
userManager!! &
;!!& '
_roleManager"" 
="" 
roleManager"" &
;""& '

JWT_SECRET## 
=## 
configuration## &
[##& '
$str##' ;
]##; <
;##< =
if$$ 
($$ 
string$$ 
.$$ 
IsNullOrWhiteSpace$$ )
($$) *

JWT_SECRET$$* 4
)$$4 5
)$$5 6
throw%% 
new%% 
	Exception%% #
(%%# $
$str%%$ <
)%%< =
;%%= >
}&& 	
[(( 	
AllowAnonymous((	 
](( 
[)) 	
HttpPost))	 
()) 
$str)) 
))) 
])) 
public** 
async** 
Task** 
<** 
IActionResult** '
>**' (
Login**) .
(**. /
[**/ 0
FromBody**0 8
]**8 9
AuthenticateModel**9 J
input**K P
)**P Q
{++ 	
var,, 
user,, 
=,, 
await,, 
_userManager,, )
.,,) *
FindByNameAsync,,* 9
(,,9 :
input,,: ?
.,,? @
Email,,@ E
),,E F
;,,F G
if.. 
(.. 
user.. 
!=.. 
null.. 
).. 
{// 
if00 
(00 
_userManager00  
.00  !
PasswordHasher00! /
.00/ 0 
VerifyHashedPassword000 D
(00D E
user00E I
,00I J
user00K O
.00O P
PasswordHash00P \
,00\ ]
input00^ c
.00c d
Password00d l
)00l m
==00n p'
PasswordVerificationResult	00q 
.
00 
Success
00 
)
00 
{11 
var22 
claims22 
=22  
(22! "
await22" '
_userManager22( 4
.224 5
GetClaimsAsync225 C
(22C D
user22D H
)22H I
)22I J
.22J K
ToList22K Q
(22Q R
)22R S
;22S T
foreach33 
(33 
var33  
roleName33! )
in33* ,
await33- 2
_userManager333 ?
.33? @
GetRolesAsync33@ M
(33M N
user33N R
)33R S
)33S T
{44 
var55 
role55  
=55! "
await55# (
_roleManager55) 5
.555 6
FindByNameAsync556 E
(55E F
roleName55F N
)55N O
;55O P
var66 

roleClaims66 &
=66' (
await66) .
_roleManager66/ ;
.66; <
GetClaimsAsync66< J
(66J K
role66K O
)66O P
;66P Q
claims77 
.77 
AddRange77 '
(77' (

roleClaims77( 2
)772 3
;773 4
}88 
claims99 
.99 
Add99 
(99 
new99 "
Claim99# (
(99( )

ClaimTypes99) 3
.993 4
Name994 8
,998 9
input99: ?
.99? @
Email99@ E
.99E F
ToString99F N
(99N O
)99O P
)99P Q
)99Q R
;99R S
var;; 
tokenHandler;; $
=;;% &
new;;' *#
JwtSecurityTokenHandler;;+ B
(;;B C
);;C D
;;;D E
var<< 
key<< 
=<< 
Encoding<< &
.<<& '
ASCII<<' ,
.<<, -
GetBytes<<- 5
(<<5 6

JWT_SECRET<<6 @
)<<@ A
;<<A B
var== 
tokenDescriptor== '
===( )
new==* -#
SecurityTokenDescriptor==. E
{>> 
Subject?? 
=??  !
new??" %
ClaimsIdentity??& 4
(??4 5
claims??5 ;
)??; <
,??< =
Expires@@ 
=@@  !
DateTime@@" *
.@@* +
UtcNow@@+ 1
.@@1 2
AddDays@@2 9
(@@9 :
$num@@: ;
)@@; <
,@@< =
SigningCredentialsAA *
=AA+ ,
newAA- 0
SigningCredentialsAA1 C
(AAC D
newAAD G 
SymmetricSecurityKeyAAH \
(AA\ ]
keyAA] `
)AA` a
,AAa b
SecurityAlgorithmsAAc u
.AAu v 
HmacSha256Signature	AAv 
)
AA 
,
AA 
}BB 
;BB 
varDD 
tokenDD 
=DD 
tokenHandlerDD  ,
.DD, -"
CreateJwtSecurityTokenDD- C
(DDC D
tokenDescriptorDDD S
)DDS T
;DDT U
varEE 
tokenStringEE #
=EE$ %
tokenHandlerEE& 2
.EE2 3

WriteTokenEE3 =
(EE= >
tokenEE> C
)EEC D
;EED E
varFF 
tokenResultFF #
=FF$ %
newFF& )
TokenResultFF* 5
{GG 

auth_tokenHH "
=HH# $
tokenStringHH% 0
}II 
;II 
returnKK 
OkKK 
(KK 
tokenResultKK )
)KK) *
;KK* +
}LL 
}MM 
returnOO 
UnauthorizedOO 
(OO  
)OO  !
;OO! "
}PP 	
[RR 	
HttpGetRR	 
(RR 
$strRR 
)RR 
]RR 
publicSS 
IActionResultSS 
UserInfoSS %
(SS% &
)SS& '
{TT 	
varUU 
uidUU 
=UU 
UserUU 
.UU 
IdentityUU #
.UU# $
NameUU$ (
;UU( )
returnVV 
OkVV 
(VV 
uidVV 
)VV 
;VV 
}WW 	
[YY 	
HttpGetYY	 
(YY 
$strYY 
)YY 
]YY 
[ZZ 	
ClaimAuthorizeZZ	 
(ZZ 
	SysClaimsZZ !
.ZZ! "
SystemManageZZ" .
)ZZ. /
]ZZ/ 0
public[[ 
IActionResult[[ 
	SysManage[[ &
([[& '
)[[' (
{\\ 	
return]] 
Ok]] 
(]] 
$str]] "
)]]" #
;]]# $
}^^ 	
[`` 	
HttpGet``	 
(`` 
$str`` 
)`` 
]`` 
[aa 	
ClaimAuthorizeaa	 
(aa 
	SysClaimsaa !
.aa! "

BookManageaa" ,
)aa, -
]aa- .
publicbb 
IActionResultbb 

BookManagebb '
(bb' (
)bb( )
{cc 	
returndd 
Okdd 
(dd 
$strdd "
)dd" #
;dd# $
}ee 	
}ff 
publicjj 

classjj 
AuthenticateModeljj "
{kk 
[ll 	
Requiredll	 
]ll 
publicmm 
stringmm 
Emailmm 
{mm 
getmm !
;mm! "
setmm# &
;mm& '
}mm( )
[oo 	
Requiredoo	 
]oo 
publicpp 
stringpp 
Passwordpp 
{pp  
getpp! $
;pp$ %
setpp& )
;pp) *
}pp+ ,
}qq 
publicss 

classss 
TokenResultss 
{tt 
publicuu 
stringuu 

auth_tokenuu  
{uu! "
getuu# &
;uu& '
setuu( +
;uu+ ,
}uu- .
}vv 
}xx 
RC:\git_troy\ShindaBookStore\BookStore.API\Controllers\WeatherForecastController.cs
	namespace 	
	BookStore
 
. 
API 
. 
Controllers #
{		 
[

 
ApiController

 
]

 
[ 
Route 

(
 
$str 
) 
] 
public 

class %
WeatherForecastController *
:+ ,
ControllerBase- ;
{ 
private 
static 
readonly 
string  &
[& '
]' (
	Summaries) 2
=3 4
new5 8
[8 9
]9 :
{ 	
$str 
, 
$str !
,! "
$str# +
,+ ,
$str- 3
,3 4
$str5 ;
,; <
$str= C
,C D
$strE L
,L M
$strN S
,S T
$strU a
,a b
$strc n
} 	
;	 

private 
readonly 
ILogger  
<  !%
WeatherForecastController! :
>: ;
_logger< C
;C D
public %
WeatherForecastController (
(( )
ILogger) 0
<0 1%
WeatherForecastController1 J
>J K
loggerL R
)R S
{ 	
_logger 
= 
logger 
; 
} 	
[ 	
HttpGet	 
] 
public 
IEnumerable 
< 
WeatherForecast *
>* +
Get, /
(/ 0
)0 1
{ 	
var 
rng 
= 
new 
Random  
(  !
)! "
;" #
return 

Enumerable 
. 
Range #
(# $
$num$ %
,% &
$num' (
)( )
.) *
Select* 0
(0 1
index1 6
=>7 9
new: =
WeatherForecast> M
{ 
Date   
=   
DateTime   
.    
Now    #
.  # $
AddDays  $ +
(  + ,
index  , 1
)  1 2
,  2 3
TemperatureC!! 
=!! 
rng!! "
.!!" #
Next!!# '
(!!' (
-!!( )
$num!!) +
,!!+ ,
$num!!- /
)!!/ 0
,!!0 1
Summary"" 
="" 
	Summaries"" #
[""# $
rng""$ '
.""' (
Next""( ,
("", -
	Summaries""- 6
.""6 7
Length""7 =
)""= >
]""> ?
}## 
)## 
.$$ 
ToArray$$ 
($$ 
)$$ 
;$$ 
}%% 	
}&& 
}'' í
?C:\git_troy\ShindaBookStore\BookStore.API\Data\DbInitializer.cs
	namespace

 	
	BookStore


 
.

 
API

 
.

 
Data

 
{ 
public 

class 
DbInitializer 
{ 
public 
static 
void 

Initialize %
(% &
BookStoreDbContext& 8
context9 @
,@ A
UserManagerB M
<M N
IdentityUserN Z
>Z [
userManager\ g
,g h
RoleManageri t
<t u
IdentityRole	u 
>
 
roleManager
 
)
 
{ 	
context 
. 
Database 
. 
EnsureCreated *
(* +
)+ ,
;, -
	SetupData 
( 
userManager !
,! "
roleManager# .
). /
./ 0
ConfigureAwait0 >
(> ?
false? D
)D E
.E F

GetAwaiterF P
(P Q
)Q R
.R S
	GetResultS \
(\ ]
)] ^
;^ _
} 	
static 
async 
Task 
	SetupData #
(# $
UserManager$ /
</ 0
IdentityUser0 <
>< =
userManager> I
,I J
RoleManagerK V
<V W
IdentityRoleW c
>c d
roleManagere p
)p q
{ 	
var 

adminEmail 
= 
$str -
;- .
if 
( 
await 
userManager !
.! "
FindByEmailAsync" 2
(2 3

adminEmail3 =
)= >
==? A
nullB F
)F G
{ 
var 
	adminUser 
= 
new  #
IdentityUser$ 0
{ 
UserName 
= 

adminEmail )
,) *
Email 
= 

adminEmail &
,& '
} 
; 
using!! 
(!! 
var!! 
scope!!  
=!!! "
new!!# &
TransactionScope!!' 7
(!!7 8+
TransactionScopeAsyncFlowOption!!8 W
.!!W X
Enabled!!X _
)!!_ `
)!!` a
{"" 
var$$ 
	adminRole$$ !
=$$" #
new$$$ '
IdentityRole$$( 4
($$4 5
$str$$5 B
)$$B C
;$$C D
await%% 
roleManager%% %
.%%% &
CreateAsync%%& 1
(%%1 2
	adminRole%%2 ;
)%%; <
;%%< =
await&& 
roleManager&& %
.&&% &
AddClaimAsync&&& 3
(&&3 4
	adminRole&&4 =
,&&= >
new&&? B
Claim&&C H
(&&H I
	SysClaims&&I R
.&&R S
SystemManage&&S _
,&&_ `
$str&&a c
)&&c d
)&&d e
;&&e f
await** 
userManager** %
.**% &
CreateAsync**& 1
(**1 2
	adminUser**2 ;
,**; <
$str**= G
)**G H
;**H I
await++ 
userManager++ %
.++% &
AddToRoleAsync++& 4
(++4 5
	adminUser++5 >
,++> ?
	adminRole++@ I
.++I J
Name++J N
)++N O
;++O P
scope-- 
.-- 
Complete-- "
(--" #
)--# $
;--$ %
}.. 
}// 
}00 	
}11 
}22 %
9C:\git_troy\ShindaBookStore\BookStore.API\DomainLogger.cs
	namespace 	
	BookStore
 
. 
API 
{		 
public

 

class

 
DomainLogger

 
<

 
T

 
>

  
:

! "
ILog

# '
<

' (
T

( )
>

) *
{ 
readonly 
ILogger 
< 
T 
> 
_logger #
;# $
public 
DomainLogger 
( 
ILoggerFactory *
factory+ 2
)2 3
{ 	
_logger 
= 
factory 
. 
CreateLogger *
<* +
T+ ,
>, -
(- .
). /
;/ 0
} 	
public 
void 
Critical 
( 
string #
msg$ '
)' (
{ 	
_logger 
. 
LogCritical 
(  
msg  #
)# $
;$ %
} 	
public 
void 
Critical 
( 
	Exception &
	exception' 0
,0 1
string2 8
msg9 <
)< =
{ 	
_logger 
. 
LogCritical 
(  
	exception  )
,) *
msg+ .
). /
;/ 0
} 	
public 
void 
Debug 
( 
string  
msg! $
)$ %
{ 	
_logger 
. 
LogDebug 
( 
msg  
)  !
;! "
} 	
public!! 
void!! 
Debug!! 
(!! 
	Exception!! #
	exception!!$ -
,!!- .
string!!/ 5
msg!!6 9
)!!9 :
{"" 	
_logger## 
.## 
LogDebug## 
(## 
	exception## &
,##& '
msg##( +
)##+ ,
;##, -
}$$ 	
public&& 
void&& 
Error&& 
(&& 
string&&  
msg&&! $
)&&$ %
{'' 	
_logger(( 
.(( 
LogError(( 
((( 
msg((  
)((  !
;((! "
})) 	
public++ 
void++ 
Error++ 
(++ 
	Exception++ #
	exception++$ -
,++- .
string++/ 5
msg++6 9
)++9 :
{,, 	
_logger-- 
.-- 
LogError-- 
(-- 
	exception-- &
,--& '
msg--( +
)--+ ,
;--, -
}.. 	
public00 
void00 
Info00 
(00 
string00 
msg00  #
)00# $
{11 	
_logger22 
.22 
LogInformation22 "
(22" #
msg22# &
)22& '
;22' (
}33 	
public55 
void55 
Info55 
(55 
	Exception55 "
	exception55# ,
,55, -
string55. 4
msg555 8
)558 9
{66 	
_logger77 
.77 
LogInformation77 "
(77" #
	exception77# ,
,77, -
msg77. 1
)771 2
;772 3
}88 	
public:: 
void:: 
Trace:: 
(:: 
string::  
msg::! $
)::$ %
{;; 	
_logger<< 
.<< 
LogTrace<< 
(<< 
msg<<  
)<<  !
;<<! "
}== 	
public?? 
void?? 
Trace?? 
(?? 
	Exception?? #
	exception??$ -
,??- .
string??/ 5
msg??6 9
)??9 :
{@@ 	
_loggerAA 
.AA 
LogTraceAA 
(AA 
	exceptionAA &
,AA& '
msgAA( +
)AA+ ,
;AA, -
}BB 	
publicDD 
voidDD 
WarnDD 
(DD 
stringDD 
msgDD  #
)DD# $
{EE 	
_loggerFF 
.FF 

LogWarningFF 
(FF 
msgFF "
)FF" #
;FF# $
}GG 	
publicII 
voidII 
WarnII 
(II 
	ExceptionII "
	exceptionII# ,
,II, -
stringII. 4
msgII5 8
)II8 9
{JJ 	
_loggerKK 
.KK 

LogWarningKK 
(KK 
	exceptionKK (
,KK( )
msgKK* -
)KK- .
;KK. /
}LL 	
}MM 
}NN 
SC:\git_troy\ShindaBookStore\BookStore.API\extensions\ServiceCollectionExtensions.cs
	namespace 	
	BookStore
 
. 
API 
{		 
public

 

static

 
class

 '
ServiceCollectionExtensions

 3
{ 
public 
static 
TOptions "
ConfigureDomainOptions 5
<5 6
TOptions6 >
>> ?
(? @
this@ D
IServiceCollectionE W
servicesX `
,` a
IConfigurationb p
configurationq ~
)~ 
where
 
TOptions
 
:
 
class
 
,
 
new
 
(
 
)
 
{ 	
if 
( 
services 
== 
null  
)  !
throw" '
new( +!
ArgumentNullException, A
(A B
nameofB H
(H I
servicesI Q
)Q R
)R S
;S T
if 
( 
configuration 
==  
null! %
)% &
throw' ,
new- 0!
ArgumentNullException1 F
(F G
nameofG M
(M N
configurationN [
)[ \
)\ ]
;] ^
var 
config 
= 
new 
TOptions %
(% &
)& '
;' (
configuration 
. 
Bind 
( 
config %
)% &
;& '
services 
. 
AddSingleton !
(! "
config" (
)( )
;) *
return 
config 
; 
} 	
public%% 
static%% 
IServiceCollection%% ("
ConfigureDomainOptions%%) ?
<%%? @
TOptions%%@ H
>%%H I
(%%I J
this%%J N
IServiceCollection%%O a
services%%b j
,%%j k
Action%%l r
<%%r s
TOptions%%s {
>%%{ |
configureOptions	%%} 
)
%% 
where
%% 
TOptions
%% 
:
%% 
class
%%  Ĩ
,
%%Ĩ Ķ
new
%%§ Š
(
%%Š Ŧ
)
%%Ŧ Ž
{&& 	
if'' 
('' 
services'' 
=='' 
null''  
)''  !
throw''" '
new''( +!
ArgumentNullException'', A
(''A B
nameof''B H
(''H I
services''I Q
)''Q R
)''R S
;''S T
if(( 
((( 
configureOptions((  
==((! #
null(($ (
)((( )
throw((* /
new((0 3!
ArgumentNullException((4 I
(((I J
nameof((J P
(((P Q
configureOptions((Q a
)((a b
)((b c
;((c d
var** 
options** 
=** 
(** 
TOptions** #
)**# $
	Activator**$ -
.**- .
CreateInstance**. <
(**< =
typeof**= C
(**C D
TOptions**D L
)**L M
)**M N
;**N O
configureOptions++ 
(++ 
options++ $
)++$ %
;++% &
services,, 
.,, 
AddSingleton,, !
(,,! "
options,," )
),,) *
;,,* +
return-- 
services-- 
;-- 
}.. 	
}// 
}00 Ŧ

4C:\git_troy\ShindaBookStore\BookStore.API\Program.cs
	namespace

 	
	BookStore


 
.

 
API

 
{ 
public 

class 
Program 
{ 
public 
static 
void 
Main 
(  
string  &
[& '
]' (
args) -
)- .
{ 	
CreateHostBuilder 
( 
args "
)" #
.# $
Build$ )
() *
)* +
.+ ,
Run, /
(/ 0
)0 1
;1 2
} 	
public 
static 
IHostBuilder "
CreateHostBuilder# 4
(4 5
string5 ;
[; <
]< =
args> B
)B C
=>D F
Host 
.  
CreateDefaultBuilder %
(% &
args& *
)* +
. $
ConfigureWebHostDefaults )
() *

webBuilder* 4
=>5 7
{ 

webBuilder 
. 

UseStartup )
<) *
Startup* 1
>1 2
(2 3
)3 4
;4 5
} 
) 
; 
} 
} Í6
4C:\git_troy\ShindaBookStore\BookStore.API\Startup.cs
	namespace 	
	BookStore
 
. 
API 
{ 
public 

class 
Startup 
{ 
public 
Startup 
( 
IConfiguration %
configuration& 3
)3 4
{ 	
Configuration 
= 
configuration )
;) *
} 	
public   
IConfiguration   
Configuration   +
{  , -
get  . 1
;  1 2
}  3 4
public## 
void## 
ConfigureServices## %
(##% &
IServiceCollection##& 8
services##9 A
)##A B
{$$ 	
var%% 

JWT_SECRET%% 
=%% 
Encoding%% %
.%%% &
ASCII%%& +
.%%+ ,
GetBytes%%, 4
(%%4 5
Configuration%%5 B
[%%B C
$str%%C W
]%%W X
)%%X Y
;%%Y Z
services'' 
.'' 
AddDbContext'' !
<''! "
BookStoreDbContext''" 4
>''4 5
(''5 6
options''6 =
=>''> @
options(( 
.(( 
UseInMemoryDatabase(( +
(((+ ,
$str((, 3
)((3 4
)((4 5
;((5 6
services++ 
.++ 
AddIdentityCore++ $
<++$ %
IdentityUser++% 1
>++1 2
(++2 3
options++3 :
=>++; =
{,, 
options-- 
.-- 
SignIn-- 
.-- #
RequireConfirmedAccount-- 6
=--7 8
false--9 >
;--> ?
options00 
.00 
Password00  
.00  !"
RequireNonAlphanumeric00! 7
=008 9
false00: ?
;00? @
}11 
)11 
.22 
AddRoles22 
<22 
IdentityRole22 "
>22" #
(22# $
)22$ %
.33 $
AddEntityFrameworkStores33 %
<33% &
BookStoreDbContext33& 8
>338 9
(339 :
)33: ;
.44 
AddSignInManager44 
(44 
)44 
.55 $
AddDefaultTokenProviders55 %
(55% &
)55& '
;55' (
services77 
.77 
AddAuthentication77 &
(77& '
x77' (
=>77) +
{88 
x99 
.99 %
DefaultAuthenticateScheme99 +
=99, -
JwtBearerDefaults99. ?
.99? @ 
AuthenticationScheme99@ T
;99T U
x:: 
.:: "
DefaultChallengeScheme:: (
=::) *
JwtBearerDefaults::+ <
.::< = 
AuthenticationScheme::= Q
;::Q R
};; 
);; 
.<< 
AddJwtBearer<< 
(<< 
x<< 
=><< 
{== 
x>> 
.>>  
RequireHttpsMetadata>> &
=>>' (
false>>) .
;>>. /
x?? 
.?? 
	SaveToken?? 
=?? 
true?? "
;??" #
x@@ 
.@@ %
TokenValidationParameters@@ +
=@@, -
new@@. 1%
TokenValidationParameters@@2 K
{AA $
ValidateIssuerSigningKeyBB ,
=BB- .
trueBB/ 3
,BB3 4
IssuerSigningKeyCC $
=CC% &
newCC' * 
SymmetricSecurityKeyCC+ ?
(CC? @

JWT_SECRETCC@ J
)CCJ K
,CCK L
ValidateIssuerDD "
=DD# $
falseDD% *
,DD* +
ValidateAudienceEE $
=EE% &
falseEE' ,
}FF 
;FF 
}GG 
)GG 
;GG 
servicesJJ 
.JJ "
ConfigureDomainOptionsJJ +
<JJ+ ,
DomainOptionsJJ, 9
>JJ9 :
(JJ: ;
ConfigurationJJ; H
.JJH I

GetSectionJJI S
(JJS T
$strJJT c
)JJc d
)JJd e
;JJe f
servicesMM 
.MM 
	AddScopedMM 
(MM 
typeofMM %
(MM% &
ILogMM& *
<MM* +
>MM+ ,
)MM, -
,MM- .
typeofMM/ 5
(MM5 6
DomainLoggerMM6 B
<MMB C
>MMC D
)MMD E
)MME F
;MMF G
servicesPP 
.PP  
AddClaimAuthoriztionPP )
(PP) *
)PP* +
;PP+ ,
servicesRR 
.RR 
AddCorsRR 
(RR 
)RR 
;RR 
servicesSS 
.SS 
AddControllersSS #
(SS# $
)SS$ %
;SS% &
}TT 	
publicWW 
voidWW 
	ConfigureWW 
(WW 
IApplicationBuilderWW 1
appWW2 5
,WW5 6
IWebHostEnvironmentWW7 J
envWWK N
,WWN O
IServiceProviderWWP `
serviceProviderWWa p
)WWp q
{XX 	
DataZZ 
.ZZ 
DbInitializerZZ 
.ZZ 

InitializeZZ )
(ZZ) *
serviceProvider[[ 
.[[  
GetRequiredService[[  2
<[[2 3
BookStoreDbContext[[3 E
>[[E F
([[F G
)[[G H
,[[H I
serviceProvider\\ 
.\\  
GetRequiredService\\  2
<\\2 3
UserManager\\3 >
<\\> ?
IdentityUser\\? K
>\\K L
>\\L M
(\\M N
)\\N O
,\\O P
serviceProvider]] 
.]]  
GetRequiredService]]  2
<]]2 3
RoleManager]]3 >
<]]> ?
IdentityRole]]? K
>]]K L
>]]L M
(]]M N
)]]N O
)^^ 
;^^ 
if`` 
(`` 
env`` 
.`` 
IsDevelopment`` !
(``! "
)``" #
)``# $
{aa 
appbb 
.bb %
UseDeveloperExceptionPagebb -
(bb- .
)bb. /
;bb/ 0
}cc 
appee 
.ee 

UseRoutingee 
(ee 
)ee 
;ee 
apphh 
.hh 
UseCorshh 
(hh 
xhh 
=>hh 
xhh 
.ii 
AllowAnyOriginii 
(ii  
)ii  !
.jj 
AllowAnyMethodjj 
(jj  
)jj  !
.kk 
AllowAnyHeaderkk 
(kk  
)kk  !
)kk! "
;kk" #
appmm 
.mm 
UseAuthorizationmm  
(mm  !
)mm! "
;mm" #
appoo 
.oo 
UseEndpointsoo 
(oo 
	endpointsoo &
=>oo' )
{pp 
	endpointsqq 
.qq 
MapControllersqq (
(qq( )
)qq) *
;qq* +
}rr 
)rr 
;rr 
}ss 	
}tt 
}uu ĩ
6C:\git_troy\ShindaBookStore\BookStore.API\SysClaims.cs
	namespace 	
	BookStore
 
. 
API 
{ 
public 

static 
class 
	SysClaims !
{		 
public

 
const

 
string

 

BookManage

 &
=

' (
$str

) 5
;

5 6
public 
const 
string 
SystemManage (
=) *
$str+ 9
;9 :
public 
static 
class 
Values "
{ 	
public 
const 
string 
Create  &
=' (
$str) 1
;1 2
public 
const 
string 
Read  $
=% &
$str' -
;- .
public 
const 
string 
Update  &
=' (
$str) 1
;1 2
public 
const 
string 
Delete  &
=' (
$str) 1
;1 2
public 
const 
string 
Approve  '
=( )
$str* 3
;3 4
} 	
public 
static 
List 
< 
( 
string "
	ClaimType# ,
,, -
string. 4
[4 5
]5 6
ClaimValues7 B
)B C
>C D
GetAllE K
(K L
)L M
{ 	
return 
new 
List 
< 
( 
string #
	ClaimType$ -
,- .
string/ 5
[5 6
]6 7
ClaimValues8 C
)C D
>D E
{ 
( 
SystemManage 
, 
new  #
string$ *
[* +
]+ ,
{, -
Values. 4
.4 5
Create5 ;
,; <
Values= C
.C D
ReadD H
,H I
ValuesJ P
.P Q
UpdateQ W
}X Y
)Z [
,[ \
( 

BookManage 
, 
new !
string" (
[( )
]) *
{* +
Values, 2
.2 3
Create3 9
,9 :
Values; A
.A B
ReadB F
,F G
ValuesH N
.N O
UpdateO U
,U V
ValuesW ]
.] ^
Approve^ e
}f g
)h i
,i j
} 
; 
} 	
public 
static 

Dictionary  
<  !
string! '
,' (
string) /
[/ 0
]0 1
>1 2
Claims3 9
=: ;
new< ?

Dictionary@ J
<J K
stringK Q
,Q R
stringS Y
[Y Z
]Z [
>[ \
{   	
{!! 
SystemManage!! 
,!! 
new!! 
string!!  &
[!!& '
]!!' (
{!!( )
}!!* +
}!!, -
,!!- .
{"" 

BookManage"" 
,"" 
new"" 
string"" $
[""$ %
]""% &
{""& '
Values""( .
."". /
Create""/ 5
,""5 6
Values""7 =
.""= >
Read""> B
,""B C
Values""D J
.""J K
Update""K Q
,""Q R
Values""S Y
.""Y Z
Approve""Z a
}""b c
}""d e
,""e f
}## 	
;##	 

}$$ 
}%% ß
<C:\git_troy\ShindaBookStore\BookStore.API\WeatherForecast.cs
	namespace 	
	BookStore
 
. 
API 
{ 
public 

class 
WeatherForecast  
{ 
public 
DateTime 
Date 
{ 
get "
;" #
set$ '
;' (
}) *
public		 
int		 
TemperatureC		 
{		  !
get		" %
;		% &
set		' *
;		* +
}		, -
public 
int 
TemperatureF 
=>  "
$num# %
+& '
(( )
int) ,
), -
(- .
TemperatureC. :
/; <
$num= C
)C D
;D E
public 
string 
Summary 
{ 
get  #
;# $
set% (
;( )
}* +
} 
} 