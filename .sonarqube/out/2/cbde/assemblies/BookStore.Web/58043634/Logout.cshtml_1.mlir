func @_BookStore.Web.Areas.Identity.Pages.Account.LogoutModel.OnGet$$() -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Areas\\Identity\\Pages\\Account\\Logout.cshtml.cs" :24 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Areas\\Identity\\Pages\\Account\\Logout.cshtml.cs" :26 :12) // Not a variable of known type: _signInManager
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Areas\\Identity\\Pages\\Account\\Logout.cshtml.cs" :26 :12) // _signInManager.SignOutAsync() (InvocationExpression)
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Areas\\Identity\\Pages\\Account\\Logout.cshtml.cs" :26 :12) // _signInManager.SignOutAsync().Wait() (InvocationExpression)
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Areas\\Identity\\Pages\\Account\\Logout.cshtml.cs" :27 :12) // Not a variable of known type: _logger
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Areas\\Identity\\Pages\\Account\\Logout.cshtml.cs" :27 :35) // "User logged out." (StringLiteralExpression)
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Areas\\Identity\\Pages\\Account\\Logout.cshtml.cs" :27 :12) // _logger.LogInformation("User logged out.") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnPost(none), it contains poisonous unsupported syntaxes

