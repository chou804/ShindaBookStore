func @_BookStore.Web.Repositories.MemoryCacheBookRepository.Delete$BookStore.Domain.Models.Book$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :27 :8) {
^entry (%_entityToDelete : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :27 :27)
cbde.store %_entityToDelete, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :27 :27)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :29 :12) // Not a variable of known type: _bookRepository
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :29 :35) // Not a variable of known type: entityToDelete
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :29 :12) // _bookRepository.Delete(entityToDelete) (InvocationExpression)
%4 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :30 :12) // Not a variable of known type: _cache
%5 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :30 :26) // Not a variable of known type: entityToDelete
%6 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :30 :26) // entityToDelete.Id (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :30 :12) // _cache.Remove(entityToDelete.Id) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_BookStore.Web.Repositories.MemoryCacheBookRepository.Delete$string$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :33 :8) {
^entry (%_uid : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :33 :27)
cbde.store %_uid, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :33 :27)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :35 :12) // Not a variable of known type: _bookRepository
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :35 :35) // Not a variable of known type: uid
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :35 :12) // _bookRepository.Delete(uid) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_BookStore.Web.Repositories.MemoryCacheBookRepository.Get$System.Linq.Expressions.Expression$System.Func$BookStore.Domain.Models.Book.bool$$$(none) -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :38 :8) {
^entry (%_filter : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :38 :36)
cbde.store %_filter, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :38 :36)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :40 :19) // Not a variable of known type: _bookRepository
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :40 :39) // Not a variable of known type: filter
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :40 :19) // _bookRepository.Get(filter) (InvocationExpression)
return %3 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :40 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function GetByID(none), it contains poisonous unsupported syntaxes

func @_BookStore.Web.Repositories.MemoryCacheBookRepository.Insert$BookStore.Domain.Models.Book$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :60 :8) {
^entry (%_entity : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :60 :27)
cbde.store %_entity, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :60 :27)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :62 :12) // Not a variable of known type: _bookRepository
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :62 :35) // Not a variable of known type: entity
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :62 :12) // _bookRepository.Insert(entity) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_BookStore.Web.Repositories.MemoryCacheBookRepository.SaveChanges$$() -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :65 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :67 :12) // Not a variable of known type: _bookRepository
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :67 :12) // _bookRepository.SaveChanges() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_BookStore.Web.Repositories.MemoryCacheBookRepository.SaveChangesAsync$$() -> none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :70 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :72 :19) // Not a variable of known type: _bookRepository
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :72 :19) // _bookRepository.SaveChangesAsync() (InvocationExpression)
return %1 : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :72 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_BookStore.Web.Repositories.MemoryCacheBookRepository.Update$BookStore.Domain.Models.Book$(none) -> () loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :75 :8) {
^entry (%_entityToUpdate : none):
%0 = cbde.alloca none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :75 :27)
cbde.store %_entityToUpdate, %0 : memref<none> loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :75 :27)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :77 :12) // Not a variable of known type: _bookRepository
%2 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :77 :35) // Not a variable of known type: entityToUpdate
%3 = cbde.unknown : none loc("C:\\git_troy\\ShindaBookStore\\BookStore.Web\\Repositories\\MemoryCacheBookRepository.cs" :77 :12) // _bookRepository.Update(entityToUpdate) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
