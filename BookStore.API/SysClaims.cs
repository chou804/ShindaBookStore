﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.API
{
    public static class SysClaims
    {
        public const string BookManage = "BookManage";
        public const string SystemManage = "SystemManage";

        public static class Values
        {
            public const string Create = "Create";
            public const string Read = "Read";
            public const string Update = "Update";
            public const string Delete = "Delete";
            public const string Approve = "Approve";
        }

        public static List<(string ClaimType, string[] ClaimValues)> GetAll()
        {
            return new List<(string ClaimType, string[] ClaimValues)>
            {
                ( SystemManage, new string[]{ Values.Create, Values.Read, Values.Update } ),
                ( BookManage, new string[]{ Values.Create, Values.Read, Values.Update, Values.Approve } ),
            };
        }

        public static Dictionary<string, string[]> Claims = new Dictionary<string, string[]>
        {
            { SystemManage, new string[]{ } },
            { BookManage, new string[]{ Values.Create, Values.Read, Values.Update, Values.Approve } },
        };
    }
}
