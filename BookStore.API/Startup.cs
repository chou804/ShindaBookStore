using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Domain;
using BookStore.Domain.Options;
using BookStore.Domain.Repositories;
using BookStore.Domain.Services;
using BookStore.Repository.EntityFramework;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace BookStore.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var JWT_SECRET = Encoding.ASCII.GetBytes(Configuration["AppSettings:Secret"]);

            services.AddDbContext<BookStoreDbContext>(options =>
                options.UseInMemoryDatabase("memdb"));

            /*設定 Identity，使用預設的 IdentityUser及IdentityRole 物件 */
            services.AddIdentityCore<IdentityUser>(options =>
            {
                options.SignIn.RequireConfirmedAccount = false;

                // 設定User密碼複雜度
                options.Password.RequireNonAlphanumeric = false;
            })
            .AddRoles<IdentityRole>()
            .AddEntityFrameworkStores<BookStoreDbContext>()
            .AddSignInManager()
            .AddDefaultTokenProviders();

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(JWT_SECRET),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            /*從 appsettings.json 讀取 DomainOptions 設定值*/
            services.ConfigureDomainOptions<DomainOptions>(Configuration.GetSection("DomainOptions"));

            /*註冊 DomainLogger 讓 domain project 可以使用 ILog*/
            services.AddScoped(typeof(ILog<>), typeof(DomainLogger<>));

            /*加入 ClaimAuthoriztion 權限控管*/
            services.AddClaimAuthoriztion();

            services.AddCors();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            //初始化資料庫
            Data.DbInitializer.Initialize(
                serviceProvider.GetRequiredService<BookStoreDbContext>(),
                serviceProvider.GetRequiredService<UserManager<IdentityUser>>(),
                serviceProvider.GetRequiredService<RoleManager<IdentityRole>>()
                );

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
