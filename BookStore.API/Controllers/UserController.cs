﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace BookStore.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly UserManager<IdentityUser> _userManager;
        readonly RoleManager<IdentityRole> _roleManager;
        readonly string JWT_SECRET;

        public UserController(
            IConfiguration configuration,
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager
            )
        {
            _userManager = userManager;
            _roleManager = roleManager;
            JWT_SECRET = configuration["AppSettings:Secret"];
            if (string.IsNullOrWhiteSpace(JWT_SECRET))
                throw new Exception("cannot read JWT_SECRET");
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody]AuthenticateModel input)
        {
            var user = await _userManager.FindByNameAsync(input.Email);

            if (user != null)
            {
                if (_userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, input.Password) == PasswordVerificationResult.Success)
                {
                    var claims = (await _userManager.GetClaimsAsync(user)).ToList();
                    foreach (var roleName in await _userManager.GetRolesAsync(user))
                    {
                        var role = await _roleManager.FindByNameAsync(roleName);
                        var roleClaims = await _roleManager.GetClaimsAsync(role);
                        claims.AddRange(roleClaims);
                    }
                    claims.Add(new Claim(ClaimTypes.Name, input.Email.ToString()));

                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(JWT_SECRET);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(claims),
                        Expires = DateTime.UtcNow.AddDays(7),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                    };

                    var token = tokenHandler.CreateJwtSecurityToken(tokenDescriptor);
                    var tokenString = tokenHandler.WriteToken(token);
                    var tokenResult = new TokenResult
                    {
                        auth_token = tokenString
                    };

                    return Ok(tokenResult);
                }
            }

            return Unauthorized();
        }

        [HttpGet("UserInfo")]
        public IActionResult UserInfo()
        {
            var uid = User.Identity.Name;
            return Ok(uid);
        }

        [HttpGet("SysManager")]
        [ClaimAuthorize(SysClaims.SystemManage)]
        public IActionResult SysManage()
        {
            return Ok("SysManager");
        }

        [HttpGet("BookManage")]
        [ClaimAuthorize(SysClaims.BookManage)]
        public IActionResult BookManage()
        {
            return Ok("BookManage");
        }
    }


    #region ViewModels
    public class AuthenticateModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }

    public class TokenResult
    {
        public string auth_token { get; set; }
    }
    #endregion
}